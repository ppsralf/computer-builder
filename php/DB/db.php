<?php

namespace DB\Conn;

use PDO;

class Connection
{
    protected $conn;
    public function __construct()
    {
        $db = parse_ini_file(__DIR__ . "/../config.ini");

        $user = $db['user'];
        $pass = $db['pass'];
        $name = $db['dbname'];
        $host = $db['host'];
        $type = $db['type'];

        $this->conn = new PDO("$type:host=$host;dbname=$name", "$user", "$pass");
        $this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }
}
