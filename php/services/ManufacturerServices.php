<?php

error_reporting(E_ALL);
ini_set('display_errors', 'On');

require_once __DIR__ . "/../DB/db.php";
require_once __DIR__ . "/../data/manufacturer.php";

use \Data\Manufacturer\Manufacturer;


class ManufacturerServices extends \DB\Conn\Connection
{
    public function fetchAllData(): array
    {
        $data = array();
        $stmt = $this->conn->prepare("SELECT * FROM `MANUFACTURER` ORDER BY name ASC");
        $stmt->execute();
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $manufacturer = new Manufacturer($row['name']);
            $manufacturer->setID($row['id']);

            array_push($data, $manufacturer->jsonSerialize());
        }
        return $data;
    }

    public function insertData($name)
    {
        $stmt = $this->conn->prepare("INSERT INTO `MANUFACTURER`(`name`) VALUES (:name)");
        $stmt->bindParam(':name', $name);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteData($name)
    {
        $stmt = $this->conn->prepare("DELETE FROM `MANUFACTURER` WHERE name = :name");
        $stmt->bindParam(':name', $name);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getOBJbyID($id): Manufacturer
    {
        $stmt = $this->conn->prepare("SELECT * FROM `MANUFACTURER` WHERE id = :id");
        $stmt->bindParam(':id', $id);
        $stmt->execute();

        $row = $stmt->fetch(\PDO::FETCH_ASSOC);
        $manufacturer = new Manufacturer($row['name']);
        $manufacturer->setID($id);

        return $manufacturer;
    }

    public function getIDbyName($name): int
    {
        $stmt = $this->conn->prepare("SELECT * FROM `MANUFACTURER` WHERE name = :name");
        $stmt->bindParam(':name', $name);
        $stmt->execute();

        $row = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $row['id'];
    }
}
