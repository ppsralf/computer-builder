<?php

error_reporting(E_ALL);
ini_set('display_errors', 'On');

require_once __DIR__ . "/../DB/db.php";
require_once __DIR__ . "/../data/manufacturer.php";
require_once __DIR__ . "/ManufacturerServices.php";
require_once __DIR__ . "/../data/gpu.php";

use \Data\GPU\GPU;

class GPUServices extends \DB\Conn\Connection
{
    public function fetchAllData(): array
    {
        $data = array();
        $stmt = $this->conn->prepare("SELECT * FROM `GPU` ORDER BY name ASC");
        $stmt->execute();
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $MS = new ManufacturerServices();
            $manufacturer = $MS->getOBJbyID($row['manufacturer']);

            $GPU = new GPU($row['name'], $manufacturer, $row['chipset'], $row['mem_type'], $row['TDP']);
            $GPU->setID($row['id']);

            array_push($data, $GPU->jsonSerialize());
        }
        return $data;
    }

    public function insertData(string $name, int $manufacturer_id, string $chipset, string $mem_type, int $TDP)
    {
        $stmt = $this->conn->prepare("INSERT INTO `GPU`(`name`, `manufacturer`, `chipset`, `mem_type`, `TDP`) VALUES (:name, :manufacturer_id, :chipset, :mem_type, :TDP)");
        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':manufacturer_id', $manufacturer_id);
        $stmt->bindParam(':chipset', $chipset);
        $stmt->bindParam(':mem_type', $mem_type);
        $stmt->bindParam(':TDP', $TDP);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteData($id)
    {
        $stmt = $this->conn->prepare("DELETE FROM `GPU` WHERE id = :id");
        $stmt->bindParam(':id', $id);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getInfo($id)
    {
        $stmt = $this->conn->prepare("SELECT `name`, `TDP` FROM `GPU` WHERE id = :id");
        $stmt->bindParam(':id', $id);
        $stmt->execute();

        $row = $stmt->fetch(\PDO::FETCH_ASSOC);

        return ['name' => $row['name'], 'wattage' => $row['TDP']];
    }
}
