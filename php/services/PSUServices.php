<?php

error_reporting(E_ALL);
ini_set('display_errors', 'On');

require_once __DIR__ . "/../DB/db.php";
require_once __DIR__ . "/../data/manufacturer.php";
require_once __DIR__ . "/ManufacturerServices.php";
require_once __DIR__ . "/../data/psu.php";

use Data\PSU\PSU;

class PSUServices extends \DB\Conn\Connection
{
   public function fetchAllData(): array
   {
      $data = array();
      $stmt = $this->conn->prepare("SELECT * FROM `PSU`");
      $stmt->execute();
      while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
         $MS = new ManufacturerServices();
         $manufacturer = $MS->getOBJbyID($row['manufacturer']);

         $PSU = new PSU($row['name'], $manufacturer, $row['wattage']);
         $PSU->setID($row['id']);

         array_push($data, $PSU->jsonSerialize());
      }
      return $data;
   }


   public function insertData(string $name, int $manufacturer_id, int $wattage)
   {
      $stmt = $this->conn->prepare("INSERT INTO `PSU`(`name`, `manufacturer`, `wattage`) VALUES (:name, :manufacturer_id, :wattage)");
      $stmt->bindParam(':name', $name);
      $stmt->bindParam(':manufacturer_id', $manufacturer_id);
      $stmt->bindParam(':wattage', $wattage);
      $stmt->execute();

      if ($stmt->rowCount() > 0) {
         return true;
      } else {
         return false;
      }
   }

   public function deleteData($id)
   {
      $stmt = $this->conn->prepare("DELETE FROM `PSU` WHERE id = :id");
      $stmt->bindParam(':id', $id);
      $stmt->execute();

      if ($stmt->rowCount() > 0) {
         return true;
      } else {
         return false;
      }
   }

   public function getName($id)
   {
      $stmt = $this->conn->prepare("SELECT `name`, `wattage` FROM `PSU` WHERE id = :id");
      $stmt->bindParam(':id', $id);
      $stmt->execute();

      $row = $stmt->fetch(\PDO::FETCH_ASSOC);

      return ['name' => $row['name'], 'wattage' => $row['wattage']];
   }
}
