<?php

error_reporting(E_ALL);
ini_set('display_errors', 'On');

require_once __DIR__ . "/../DB/db.php";
require_once __DIR__ . "/../data/manufacturer.php";
require_once __DIR__ . "/ManufacturerServices.php";
require_once __DIR__ . "/../data/memory.php";

use Data\Memory\Memory;

class MemoryServices extends \DB\Conn\Connection
{
    public function fetchAllData(): array
    {
        $data = array();
        $stmt = $this->conn->prepare("SELECT * FROM `MEMORY` ORDER BY name ASC");
        $stmt->execute();
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $MS = new ManufacturerServices();
            $manufacturer = $MS->getOBJbyID($row['manufacturer']);

            $Memory = new Memory($row['name'], $manufacturer, $row['type'], $row['modules'], $row['size']);
            $Memory->setID($row['id']);

            array_push($data, $Memory->jsonSerialize());
        }
        return $data;
    }

    public function insertData(string $name, int $manufacturer_id, string $type, int $modules, int $size)
    {
        $stmt = $this->conn->prepare("INSERT INTO `MEMORY`(`name`, `manufacturer`, `type`, `modules`, `size`) VALUES (:name, :manufacturer_id, :type, :modules, :size)");
        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':manufacturer_id', $manufacturer_id);
        $stmt->bindParam(':type', $type);
        $stmt->bindParam(':modules', $modules);
        $stmt->bindParam(':size', $size);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteData($id)
    {
        $stmt = $this->conn->prepare("DELETE FROM `MEMORY` WHERE id = :id");
        $stmt->bindParam(':id', $id);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getMemoryName($id)
    {
        $stmt = $this->conn->prepare("SELECT `name` FROM `MEMORY` WHERE id = :id");
        $stmt->bindParam(':id', $id);
        $stmt->execute();

        $row = $stmt->fetch(\PDO::FETCH_ASSOC);

        return $row['name'];
    }
}
