<?php

error_reporting(E_ALL);
ini_set('display_errors', 'On');

require_once __DIR__ . "/../DB/db.php";
require_once __DIR__ . "/../data/manufacturer.php";
require_once __DIR__ . "/ManufacturerServices.php";
require_once __DIR__ . "/../data/storage.php";

use Data\Storage\Storage;

class StorageServices extends \DB\Conn\Connection
{
    public function fetchAllData(): array
    {
        $data = array();
        $stmt = $this->conn->prepare("SELECT * FROM `STORAGE` ORDER BY name ASC");
        $stmt->execute();
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $MS = new ManufacturerServices();
            $manufacturer = $MS->getOBJbyID($row['manufacturer']);

            $Storage = new Storage($row['name'], $manufacturer, $row['capacity'], $row['type'], $row['form_factor']);
            $Storage->setID($row['id']);

            array_push($data, $Storage->jsonSerialize());
        }
        return $data;
    }

    public function insertData(string $name, int $manufacturer_id, int $capacity, string $type, string $form_factor)
    {
        $stmt = $this->conn->prepare("INSERT INTO `STORAGE`(`name`, `manufacturer`, `capacity`, `type`, `form_factor`) VALUES (:name, :manufacturer_id, :capacity, :type, :form_factor)");
        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':manufacturer_id', $manufacturer_id);
        $stmt->bindParam(':capacity', $capacity);
        $stmt->bindParam(':type', $type);
        $stmt->bindParam(':form_factor', $form_factor);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteData($id)
    {
        $stmt = $this->conn->prepare("DELETE FROM `STORAGE` WHERE id = :id");
        $stmt->bindParam(':id', $id);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getName($id)
    {
        $stmt = $this->conn->prepare("SELECT `name` FROM `STORAGE` WHERE id = :id");
        $stmt->bindParam(':id', $id);
        $stmt->execute();

        $row = $stmt->fetch(\PDO::FETCH_ASSOC);

        return $row['name'];
    }
}
