<?php

error_reporting(E_ALL);
ini_set('display_errors', 'On');

require_once __DIR__ . "/../DB/db.php";
require_once __DIR__ . "/../data/manufacturer.php";
require_once __DIR__ . "/ManufacturerServices.php";
require_once __DIR__ . "/../data/cpu.php";

use \Data\CPU\CPU;

class CPUServices extends \DB\Conn\Connection
{
    public function fetchAllData(): array
    {
        $data = array();
        $stmt = $this->conn->prepare("SELECT * FROM `CPU` ORDER BY name ASC");
        $stmt->execute();
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $MS = new ManufacturerServices();
            $manufacturer = $MS->getOBJbyID($row['manufacturer']);

            $CPU = new CPU($row['name'], $manufacturer, $row['core_count'], $row['TDP'], $row['socket'], $row['integrated_graphics'], $row['cooler']);
            $CPU->setID($row['id']);

            array_push($data, $CPU->jsonSerialize());
        }
        return $data;
    }

    public function insertData(string $name, int $manufacturer_id, int $core_count, int $TDP, string $socket, int $graphics, int $cooler)
    {
        $stmt = $this->conn->prepare("INSERT INTO `CPU`(`name`, `manufacturer`, `core_count`, `TDP`, `socket`, `integrated_graphics`, `cooler`) VALUES (:name, :manufacturer_id, :core_count, :TDP, :socket, :graphics, :cooler)");
        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':manufacturer_id', $manufacturer_id);
        $stmt->bindParam(':core_count', $core_count);
        $stmt->bindParam(':TDP', $TDP);
        $stmt->bindParam(':socket', $socket);
        $stmt->bindParam(':graphics', $graphics);
        $stmt->bindParam(':cooler', $cooler);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteData($id)
    {
        $stmt = $this->conn->prepare("DELETE FROM `CPU` WHERE id = :id");
        $stmt->bindParam(':id', $id);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getCPUInfo($id)
    {
        $stmt = $this->conn->prepare("SELECT `name`, `TDP`, `socket` FROM `CPU` WHERE id = :id");
        $stmt->bindParam(':id', $id);
        $stmt->execute();

        $row = $stmt->fetch(\PDO::FETCH_ASSOC);

        return ['name' => $row['name'], 'wattage' => $row['TDP'], 'socket' => $row['socket']];
    }
}
