<?php

error_reporting(E_ALL);
ini_set('display_errors', 'On');

require_once __DIR__ . "/../DB/db.php";
require_once __DIR__ . "/../data/manufacturer.php";
require_once __DIR__ . "/ManufacturerServices.php";
require_once __DIR__ . "/../data/computercase.php";

use \Data\ComputerCase\ComputerCase;

class CCServices extends \DB\Conn\Connection
{
    public function fetchAllData(): array
    {
        $data = array();
        $stmt = $this->conn->prepare("SELECT * FROM `COMPUTER_CASE` ORDER BY name ASC");
        $stmt->execute();
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $MS = new ManufacturerServices();
            $manufacturer = $MS->getOBJbyID($row['manufacturer']);

            $ComputerCase = new ComputerCase($row['name'], $manufacturer, $row['form_factor'], $row['2.5_bay'], $row['3.5_bay']);
            $ComputerCase->setID($row['id']);

            array_push($data, $ComputerCase->jsonSerialize());
        }
        return $data;
    }

    public function insertData(string $name, int $manufacturer_id, string $form_factor, int $bay25, int $bay35)
    {
        // Izveido datu bāzes pieprasījumu, lai ievietotu datora korpusa datus
        $stmt = $this->conn->prepare("INSERT INTO `COMPUTER_CASE`(`name`, `manufacturer`, `form_factor`, `2.5_bay`, `3.5_bay`) VALUES (:name, :manufacturer_id, :form_factor, :bay25, :bay35)");

        // Piesaista parametrus datu bāzes pieprasījumam
        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':manufacturer_id', $manufacturer_id);
        $stmt->bindParam('form_factor', $form_factor);
        $stmt->bindParam('bay25', $bay25);
        $stmt->bindParam('bay35', $bay35);

        // Izpilda datu bāzes pieprasījumu
        $stmt->execute();

        // Pārbauda, vai ievietošana bija veiksmīga, pamatojoties uz ietekmēto rindu skaitu
        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteData($id)
    {
        // Izveido datu bāzes pieprasījumu, lai dzēstu datora korpusa datus pēc ID
        $stmt = $this->conn->prepare("DELETE FROM `COMPUTER_CASE` WHERE id = :id");
        // Piesaista parametru datu bāzes pieprasījumam
        $stmt->bindParam(':id', $id);
        // Izpilda datu bāzes pieprasījumu
        $stmt->execute();
        // Pārbauda, vai dzēšana bija veiksmīga, pamatojoties uz ietekmēto rindu skaitu
        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getName($id)
    {
        // Izveido datu bāzes pieprasījumu, lai iegūtu datora korpusa nosaukumu pēc ID
        $stmt = $this->conn->prepare("SELECT `name` FROM `COMPUTER_CASE` WHERE id = :id");
        // Piesaista parametru datu bāzes pieprasījumam
        $stmt->bindParam(':id', $id);
        // Izpilda datu bāzes pieprasījumu
        $stmt->execute();
        // Iegūst rezultātu kā asociatīvo masīvu
        $row = $stmt->fetch(\PDO::FETCH_ASSOC);
        // Atgriež datora korpusa nosaukumu
        return $row['name'];
    }
}
