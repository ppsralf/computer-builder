<?php

error_reporting(E_ALL);
ini_set('display_errors', 'On');

require_once __DIR__ . "/../DB/db.php";
require_once __DIR__ . "/../data/user.php";

require_once __DIR__ . "/CCServices.php";
require_once __DIR__ . "/CPUServices.php";
require_once __DIR__ . "/GPUServices.php";
require_once __DIR__ . "/ManufacturerServices.php";
require_once __DIR__ . "/MBServices.php";
require_once __DIR__ . "/MemoryServices.php";
require_once __DIR__ . "/PSUServices.php";
require_once __DIR__ . "/StorageServices.php";


class DBServices extends \DB\Conn\Connection
{
    public function adminLogin()
    {
        // Iegūst POST datus no front-end pieprasījuma
        $_POST = json_decode(file_get_contents('php://input'), true);

        // Iegūst lietotājvārdu un paroli no POST datiem
        $username = $_POST['username'];
        $password = $_POST['password'];

        $login = false;

        // Izveido datu bāzes pieprasījumu, lai pārbaudītu lietotājvārdu un iegūtu saglabāto paroli
        $stmt = $this->conn->prepare("SELECT username, password FROM USERS WHERE username = :username");
        $stmt->bindParam(':username', $username);
        $stmt->execute();

        // Iegūst rezultātu no datu bāzes
        $result = $stmt->fetch();

        if ($result) {
            $hashedPW = $result['password'];

            // Salīdzina ievadīto paroli ar saglabāto paroli datu bāzē
            if (password_verify($password, $hashedPW)) {
                $login = true;
            } else {
                $status = array('status' => 'Incorrect password');
                echo json_encode($status);
            }
        } else {
            $status = array('status' => 'Username not found');
            echo json_encode($status);
        }

        // Atgriež rezultātu pēc paroles pārbaudes
        if ($login) {
            echo json_encode(true);
        } else {
            return json_encode(false);
        }
    }

    // USER FUNCTIONS

    public function insertUser()
    {
        // Iegūst POST datus no front-end pieprasījuma
        $_POST = json_decode(file_get_contents('php://input'), true);

        // Iegūst lietotājvārdu un paroli no POST datiem
        $username = strtolower($_POST['username']);
        $password = $_POST['password'];

        $userExists = false;

        // Izveido datu bāzes pieprasījumu, lai pārbaudītu vai lietotājvārds jau eksistē
        $stmt = $this->conn->prepare("SELECT username FROM `USERS` ORDER BY username ASC");
        $stmt->execute();

        // Pārbauda katru datu bāzes ierakstu, lai noskaidrotu vai lietotājvārds jau eksistē
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            if ($username == $row['username']) $userExists = true;
        }

        // Ja lietotājvārds jau eksistē, izvada kļūdas paziņojumu un atgriež kļūdas statusu
        if ($userExists) {
            echo '{"status":"Username taken"}';
            http_response_code(500);
            return false;
        }

        // Izveido jaunu lietotāja objektu
        $user = new \Data\User\User($username, $password);

        // Iegūst saglabāto paroli (kriptētu) no lietotāja objekta
        $hashedPW = $user->getPassword();

        // Izveido datu bāzes pieprasījumu, lai ievietotu jauno lietotāju datu bāzē
        $stmt = $this->conn->prepare("INSERT INTO `USERS` (`username`, `password`) VALUES (:username, :password)");
        $stmt->bindParam(':username', $username);
        $stmt->bindParam(':password', $hashedPW);

        // Izpilda datu bāzes pieprasījumu
        if ($stmt->execute()) {
            echo '{"status":"User created"}';
            http_response_code(200);
            return false;
        }
    }

    public function deleteUser()
    {
        // Iegūst POST datus no front-end pieprasījuma
        $_POST = json_decode(file_get_contents('php://input'), true);

        // Iegūst izvēlēto lietotāju no POST datiem
        $username = $_POST['selectedUser'];

        // Izveido datu bāzes pieprasījumu, lai dzēstu lietotāju
        $stmt = $this->conn->prepare("DELETE FROM `USERS` WHERE username = :username");
        $stmt->bindParam(':username', $username);

        // Izpilda datu bāzes pieprasījumu
        if ($stmt->execute()) {
            echo '{"status":"User deleted"}';
            http_response_code(200);
            return true;
        } else {
            echo '{"status":"An error occurred"}';
            http_response_code(500);
            return false;
        }
    }

    public function fetchAllUsers()
    {
        $users = array();
        $stmt = $this->conn->prepare("SELECT username FROM `USERS` ORDER BY username ASC");
        $stmt->execute();
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            array_push($users, $row['username']);
        }
        echo json_encode($users);
    }

    // COMPUTER CASE FUNCTIONS

    public function insertComputerCase()
    {
        // Iegūst POST datus no front-end pieprasījuma
        $_POST = json_decode(file_get_contents('php://input'), true);

        // Iegūst datora korpusa nosaukumu, ražotāja nosaukumu, formas faktoru un atmiņas slotu skaitus no POST datiem
        $name = $_POST['name'];
        $manufacturer_name = $_POST['manufacturer'];
        $form_factor = $_POST['formRactor'];
        $bay25 = $_POST['bay25'];
        $bay35 = $_POST['bay35'];

        // Iegūst ražotāja ID pēc nosaukuma, izmantojot ražotāja servisu
        $CCServices = new CCServices();
        $manufacturerServices = new ManufacturerServices();

        $manufacturer_id = $manufacturerServices->getIDbyName($manufacturer_name);

        $dataExists = false;

        // Izveido datu bāzes pieprasījumu, lai pārbaudītu vai dati jau eksistē
        $stmt = $this->conn->prepare("SELECT * FROM `COMPUTER_CASE`");
        $stmt->execute();

        // Pārbauda katru datu bāzes ierakstu, lai noskaidrotu vai dati jau eksistē
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            if ($name == $row['name'] && $manufacturer_id == $row['manufacturer'] && $form_factor == $row['form_factor'] && $bay25 == $row['2.5_bay'] && $bay35 == $row['3.5_bay']) $dataExists = true;
        }

        // Ja dati jau eksistē, izvada kļūdas paziņojumu un atgriež kļūdas statusu
        if ($dataExists) {
            echo '{"status":"Duplicate detected"}';
            http_response_code(500);
            return false;
        }

        // Ievieto datora korpusa datus datu bāzē, izmantojot datora korpusa servisu
        if ($CCServices->insertData($name, $manufacturer_id, $form_factor, $bay25, $bay35)) {
            echo '{"status":"Computer Case added"}';
            http_response_code(200);
            return true;
        } else {
            echo '{"status":"An error occurred"}';
            http_response_code(500);
            return false;
        }
    }

    public function deleteComputerCase()
    {
        // Iegūst POST datus no front-end pieprasījuma
        $_POST = json_decode(file_get_contents('php://input'), true);

        // Iegūst datora korpusa ID no POST datiem
        $id = $_POST['CCID'];

        $CCServices = new CCServices();

        // Izdzēš datora korpusa datus, izmantojot datora korpusa servisu
        if ($CCServices->deleteData($id)) {
            echo '{"status":"Computer Case deleted"}';
            http_response_code(200);
            return true;
        } else {
            echo '{"status":"An error occurred"}';
            http_response_code(500);
            return false;
        }
    }

    public function fetchAllComputerCases()
    {
        $CCServices = new CCServices();
        // Iegūst visus datora korpusa datus, izmantojot datora korpusa servisu, un izvada tos kā JSON
        echo json_encode($CCServices->fetchAllData());
    }

    public function getCCInfo()
    {
        if (!empty(file_get_contents('php://input'))) {
            $id = (int) file_get_contents('php://input');

            $services = new CCServices();

            echo json_encode($services->getName($id));
        }
    }

    // CPU FUNCTIONS

    public function insertCPU()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);

        $name = $_POST['name']; // cpu name
        $manufacturer_name = $_POST['manufacturer']; // manufacturer name
        $core_count = $_POST['coreCount']; // core count
        $TDP = $_POST['TDP']; // tdp
        $socket = $_POST['socket']; // socket
        $graphics = $_POST['iGraphics']; // has graphics 1 or 0
        $cooler = $_POST['cooler']; // has cooler 1 or 0

        $CPUServices = new CPUServices();
        $manufacturerServices = new ManufacturerServices();

        $manufacturer_id = $manufacturerServices->getIDbyName($manufacturer_name);

        $dataExists = false;

        $stmt = $this->conn->prepare("SELECT * FROM `CPU`");
        $stmt->execute();
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            if ($name == $row['name'] && $core_count == $row['core_count'] && $socket == $row['socket']) $dataExists = true;
        }

        if ($dataExists) {
            echo '{"status":"Duplicate detected"}';
            http_response_code(500);
            return false;
        }

        if ($CPUServices->insertData($name, $manufacturer_id, $core_count, $TDP, $socket, $graphics, $cooler)) {
            echo '{"status":"Manufacturer added"}';
            http_response_code(200);
            return true;
        } else {
            echo '{"status":"An error occurred"}';
            http_response_code(500);
            return false;
        }
    }

    public function deleteCPU()
    {
        # needs cpu id
        $_POST = json_decode(file_get_contents('php://input'), true);

        $id = $_POST['cpuID'];

        $CPUServices = new CPUServices();

        if ($CPUServices->deleteData($id)) {
            echo '{"status":"CPU deleted"}';
            http_response_code(200);
            return true;
        } else {
            echo '{"status":"An error occurred"}';
            http_response_code(500);
            return false;
        }
    }

    public function fetchAllCPUs()
    {
        $CPUServices = new CPUServices();
        echo json_encode($CPUServices->fetchAllData());
    }

    public function getCPUInfo()
    {
        if (!empty(file_get_contents('php://input'))) {
            $id = (int) file_get_contents('php://input');

            $CPUServices = new CPUServices();

            echo json_encode($CPUServices->getCPUInfo($id));
        }
    }

    // GPU FUNCTIONS

    public function insertGPU()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);

        $name = $_POST['name'];
        $manufacturer_name = $_POST['manufacturer'];
        $chipset = $_POST['chipset'];
        $mem_type = $_POST['memType'];
        $TDP = $_POST['TDP'];

        $GPUServices = new GPUServices();
        $manufacturerServices = new ManufacturerServices();

        $manufacturer_id = $manufacturerServices->getIDbyName($manufacturer_name);

        $dataExists = false;

        $stmt = $this->conn->prepare("SELECT * FROM `GPU`");
        $stmt->execute();
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            if ($name == $row['name'] && $manufacturer_id == $row['manufacturer'] && $TDP == $row['TDP']) $dataExists = true;
        }

        if ($dataExists) {
            echo '{"status":"Duplicate detected"}';
            http_response_code(500);
            return false;
        }

        if ($GPUServices->insertData($name, $manufacturer_id, $chipset, $mem_type, $TDP)) {
            echo '{"status":"GPU added"}';
            http_response_code(200);
            return true;
        } else {
            echo '{"status":"An error occurred"}';
            http_response_code(500);
            return false;
        }
    }

    public function deleteGPU()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);

        $id = $_POST['GPUID'];

        $GPUServices = new GPUServices();

        if ($GPUServices->deleteData($id)) {
            echo '{"status":"GPU deleted"}';
            http_response_code(200);
            return true;
        } else {
            echo '{"status":"An error occurred"}';
            http_response_code(500);
            return false;
        }
    }

    public function fetchAllGPUs()
    {
        $GPUServices = new GPUServices();
        echo json_encode($GPUServices->fetchAllData());
    }

    public function getGPUInfo()
    {
        if (!empty(file_get_contents('php://input'))) {
            $id = (int) file_get_contents('php://input');

            $services = new GPUServices();

            echo json_encode($services->getInfo($id));
        }
    }

    // MANUFACTURER FUNCTIONS

    public function insertManufacturer()
    {
        // needs manufacturer name
        $_POST = json_decode(file_get_contents('php://input'), true);

        $name = $_POST['name'];

        $manufacturerServices = new ManufacturerServices();

        $dataExists = false;

        $stmt = $this->conn->prepare("SELECT name FROM `MANUFACTURER`");
        $stmt->execute();
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            if ($name == $row['name']) $dataExists = true;
        }

        if ($dataExists) {
            echo '{"status":"Duplicate detected"}';
            http_response_code(500);
            return false;
        }

        if ($manufacturerServices->insertData($name)) {
            echo '{"status":"Manufacturer added"}';
            http_response_code(200);
            return true;
        } else {
            echo '{"status":"An error occurred"}';
            http_response_code(500);
            return false;
        }
    }

    public function deleteManufacturer()
    {
        // needs manufacturer name
        $_POST = json_decode(file_get_contents('php://input'), true);

        $name = $_POST['selected'];

        $manufacturerServices = new ManufacturerServices();

        if ($manufacturerServices->deleteData($name)) {
            echo '{"status":"Manufacturer deleted"}';
            http_response_code(200);
            return true;
        } else {
            echo '{"status":"An error occurred"}';
            http_response_code(500);
            return false;
        }
    }

    public function fetchAllManufacturers()
    {
        $manufacturerServices = new ManufacturerServices();
        echo json_encode($manufacturerServices->fetchAllData());
    }

    // MOTHERBOARD FUNCTIONS

    public function insertMotherboard()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);

        $name = $_POST['name'];
        $manufacturer_name = $_POST['manufacturer'];
        $socket = $_POST['socket'];
        $form_factor = $_POST['formFactor'];
        $mem_max = $_POST['memMax'];
        $mem_type = $_POST['memType'];
        $mem_slots = $_POST['memSlots'];
        $sata_ports = $_POST['sataPorts'];
        $wifi = $_POST['wifi'];

        $MBServices = new MBServices();
        $manufacturerServices = new ManufacturerServices();

        $manufacturer_id = $manufacturerServices->getIDbyName($manufacturer_name);

        $dataExists = false;

        $stmt = $this->conn->prepare("SELECT * FROM `MOTHERBOARD`");
        $stmt->execute();
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            if ($name == $row['name'] && $manufacturer_id == $row['manufacturer'] && $socket == $row['socket'] && $form_factor == $row['form_factor']) $dataExists = true;
        }

        if ($dataExists) {
            echo '{"status":"Duplicate detected"}';
            http_response_code(500);
            return false;
        }

        if ($MBServices->insertData($name, $manufacturer_id, $socket, $form_factor, $mem_max, $mem_type, $mem_slots, $sata_ports, $wifi)) {
            echo '{"status":"Motherboard added"}';
            http_response_code(200);
            return true;
        } else {
            echo '{"status":"An error occurred"}';
            http_response_code(500);
            return false;
        }
    }

    public function deleteMotherboard()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);

        $id = $_POST['MBID'];

        $MBServices = new MBServices();

        if ($MBServices->deleteData($id)) {
            echo '{"status":"Motherboard deleted"}';
            http_response_code(200);
            return true;
        } else {
            echo '{"status":"An error occurred"}';
            http_response_code(500);
            return false;
        }
    }

    public function fetchAllMotherboards()
    {
        $MBServices = new MBServices();
        echo json_encode($MBServices->fetchAllData());
    }

    public function getMBName()
    {
        if (!empty(file_get_contents('php://input'))) {
            $id = (int) file_get_contents('php://input');

            $MBServices = new MBServices();

            echo json_encode($MBServices->getMBName($id));
        }
    }

    // MEMORY FUNCTIONS

    public function insertMemory()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);

        $name = $_POST['name'];
        $manufacturer_name = $_POST['manufacturer'];
        $type = $_POST['type'];
        $modules = $_POST['modules'];
        $size = $_POST['size'];

        $MemoryServices = new MemoryServices();
        $manufacturerServices = new ManufacturerServices();

        $manufacturer_id = $manufacturerServices->getIDbyName($manufacturer_name);

        $dataExists = false;

        $stmt = $this->conn->prepare("SELECT * FROM `MEMORY`");
        $stmt->execute();
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            if ($name == $row['name'] && $manufacturer_id == $row['manufacturer'] && $type == $row['type'] && $modules == $row['modules']) $dataExists = true;
        }

        if ($dataExists) {
            echo '{"status":"Duplicate detected"}';
            http_response_code(500);
            return false;
        }

        if ($MemoryServices->insertData($name, $manufacturer_id, $type, $modules, $size)) {
            echo '{"status":"Memory added"}';
            http_response_code(200);
            return true;
        } else {
            echo '{"status":"An error occurred"}';
            http_response_code(500);
            return false;
        }
    }

    public function deleteMemory()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);

        $id = $_POST['memID'];

        $MemoryServices = new MemoryServices();

        if ($MemoryServices->deleteData($id)) {
            echo '{"status":"Memory deleted"}';
            http_response_code(200);
            return true;
        } else {
            echo '{"status":"An error occurred"}';
            http_response_code(500);
            return false;
        }
    }

    public function fetchAllMemory()
    {
        $MemoryServices = new MemoryServices();
        echo json_encode($MemoryServices->fetchAllData());
    }

    public function getMemoryName()
    {
        if (!empty(file_get_contents('php://input'))) {
            $id = (int) file_get_contents('php://input');

            $MemoryServices = new MemoryServices();

            echo json_encode($MemoryServices->getMemoryName($id));
        }
    }

    // PSU FUNCTIONS

    public function insertPSU()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);

        $name = $_POST['name'];
        $manufacturer_name = $_POST['manufacturer'];
        $wattage = $_POST['wattage'];

        $PSUServices = new PSUServices();
        $manufacturerServices = new ManufacturerServices();

        $manufacturer_id = $manufacturerServices->getIDbyName($manufacturer_name);

        $dataExists = false;

        $stmt = $this->conn->prepare("SELECT * FROM `PSU`");
        $stmt->execute();
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            if ($name == $row['name'] && $manufacturer_id == $row['manufacturer'] && $wattage == $row['wattage']) $dataExists = true;
        }

        if ($dataExists) {
            echo '{"status":"Duplicate detected"}';
            http_response_code(500);
            return false;
        }

        if ($PSUServices->insertData($name, $manufacturer_id, $wattage)) {
            echo '{"status":"Power Supply added"}';
            http_response_code(200);
            return true;
        } else {
            echo '{"status":"An error occurred"}';
            http_response_code(500);
            return false;
        }
    }

    public function deletePSU()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);

        $id = $_POST['PSUID'];

        $PSUServices = new PSUServices();

        if ($PSUServices->deleteData($id)) {
            echo '{"status":"Power Supply deleted"}';
            http_response_code(200);
            return true;
        } else {
            echo '{"status":"An error occurred"}';
            http_response_code(500);
            return false;
        }
    }

    public function fetchAllPSUs()
    {
        $PSUServices = new PSUServices();
        echo json_encode($PSUServices->fetchAllData());
    }

    public function getPSUName()
    {
        if (!empty(file_get_contents('php://input'))) {
            $id = (int) file_get_contents('php://input');

            $services = new PSUServices();

            echo json_encode($services->getName($id));
        }
    }

    // STORAGE FUNCTIONS

    public function insertStorage()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);

        $name = $_POST['name'];
        $manufacturer_name = $_POST['manufacturer'];
        $capacity = $_POST['capacity'];
        $type = $_POST['type'];
        $form_factor = $_POST['formFactor'];

        $StorageServices = new StorageServices();
        $manufacturerServices = new ManufacturerServices();

        $manufacturer_id = $manufacturerServices->getIDbyName($manufacturer_name);

        $dataExists = false;

        $stmt = $this->conn->prepare("SELECT * FROM `STORAGE`");
        $stmt->execute();
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            if ($name == $row['name'] && $manufacturer_id == $row['manufacturer'] && $capacity == $row['capacity'] && $type == $row['type'] && $form_factor == $row['form_factor']) $dataExists = true;
        }

        if ($dataExists) {
            echo '{"status":"Duplicate detected"}';
            http_response_code(500);
            return false;
        }

        if ($StorageServices->insertData($name, $manufacturer_id, $capacity, $type, $form_factor)) {
            echo '{"status":"Storage added"}';
            http_response_code(200);
            return true;
        } else {
            echo '{"status":"An error occurred"}';
            http_response_code(500);
            return false;
        }
    }

    public function deleteStorage()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);

        $id = $_POST['storageID'];

        $StorageServices = new StorageServices();

        if ($StorageServices->deleteData($id)) {
            echo '{"status":"Storage deleted"}';
            http_response_code(200);
            return true;
        } else {
            echo '{"status":"An error occurred"}';
            http_response_code(500);
            return false;
        }
    }

    public function fetchAllStorage()
    {
        $StorageServices = new StorageServices();
        echo json_encode($StorageServices->fetchAllData());
    }

    public function getStorageName()
    {
        if (!empty(file_get_contents('php://input'))) {
            $id = (int) file_get_contents('php://input');

            $services = new StorageServices();

            echo json_encode($services->getName($id));
        }
    }

    public function checkComponents()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);

        $CPU = $_POST['CPU'];
        $MB = $_POST['MB'];
        $RAM = $_POST['RAM'];
        $Storage = $_POST['Storage'];
        $GPU = $_POST['GPU'];
        $Case = $_POST['Case'];
        $PSU = $_POST['PSU'];

        // cpu
        $stmt = $this->conn->prepare("SELECT `TDP`, `socket` FROM `CPU` WHERE `name` = :name");
        $stmt->bindParam(':name', $CPU);
        $stmt->execute();

        $row = $stmt->fetch(\PDO::FETCH_ASSOC);

        $CPUW = $row['TDP'];
        $CPUS = $row['socket'];

        // gpu w
        $stmt = $this->conn->prepare("SELECT `TDP` FROM `GPU` WHERE `name` = :name");
        $stmt->bindParam(':name', $GPU);
        $stmt->execute();

        $row = $stmt->fetch(\PDO::FETCH_ASSOC);

        $GPUW = $row['TDP'];

        // psu w
        $stmt = $this->conn->prepare("SELECT `wattage` FROM `PSU` WHERE `name` = :name");
        $stmt->bindParam(':name', $PSU);
        $stmt->execute();

        $row = $stmt->fetch(\PDO::FETCH_ASSOC);

        $PSUW = $row['wattage'];

        // mb
        $stmt = $this->conn->prepare("SELECT `socket` FROM `MOTHERBOARD` WHERE `name` = :name");
        $stmt->bindParam(':name', $MB);
        $stmt->execute();

        $row = $stmt->fetch(\PDO::FETCH_ASSOC);

        $MBS = $row['socket'];

        $totalWattage = $CPUW + $GPUW;
        if ($Storage != "") {
            $totalWattage += 10;
        }

        $warnings = [];

        if (($GPUW + $CPUW) > $PSUW) {
            array_push($warnings, "This configuration requires a more powerful PSU.");
        }

        if ($MBS != $CPUS) {
            array_push($warnings, "The selected CPU isn't supported by the selected motherboard.");
        }

        echo json_encode(['totalWattage' => $totalWattage, 'warnings' => $warnings]);
    }
}
