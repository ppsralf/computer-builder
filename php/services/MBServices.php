<?php

error_reporting(E_ALL);
ini_set('display_errors', 'On');

require_once __DIR__ . "/../DB/db.php";
require_once __DIR__ . "/../data/manufacturer.php";
require_once __DIR__ . "/ManufacturerServices.php";
require_once __DIR__ . "/../data/motherboard.php";

use Data\Motherboard\Motherboard;

class MBServices extends \DB\Conn\Connection
{
    public function fetchAllData(): array
    {
        $data = array();
        $stmt = $this->conn->prepare("SELECT * FROM `MOTHERBOARD` ORDER BY name ASC");
        $stmt->execute();
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $MS = new ManufacturerServices();
            $manufacturer = $MS->getOBJbyID($row['manufacturer']);

            $MB = new Motherboard($row['name'], $manufacturer, $row['socket'], $row['form_factor'], $row['mem_max'], $row['mem_type'], $row['mem_slots'], $row['SATA_ports'], $row['wifi']);
            $MB->setID($row['id']);

            array_push($data, $MB->jsonSerialize());
        }
        return $data;
    }

    public function insertData(string $name, int $manufacturer_id, string $socket, string $form_factor, int $mem_max, string $mem_type, int $mem_slots, int $SATA_ports, int $wifi)
    {
        $stmt = $this->conn->prepare("INSERT INTO `MOTHERBOARD`(`name`, `manufacturer`, `socket`, `form_factor`, `mem_max`, `mem_type`, `mem_slots`, `SATA_ports`, `wifi`) VALUES (:name, :manufacturer_id, :socket, :form_factor, :mem_max, :mem_type, :mem_slots, :SATA_ports, :wifi)");
        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':manufacturer_id', $manufacturer_id);
        $stmt->bindParam(':socket', $socket);
        $stmt->bindParam(':form_factor', $form_factor);
        $stmt->bindParam(':mem_max', $mem_max);
        $stmt->bindParam(':mem_type', $mem_type);
        $stmt->bindParam(':mem_slots', $mem_slots);
        $stmt->bindParam(':SATA_ports', $SATA_ports);
        $stmt->bindParam(':wifi', $wifi);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteData($id)
    {
        $stmt = $this->conn->prepare("DELETE FROM `MOTHERBOARD` WHERE id = :id");
        $stmt->bindParam(':id', $id);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getMBName($id)
    {
        $stmt = $this->conn->prepare("SELECT `name`, `socket` FROM `MOTHERBOARD` WHERE id = :id");
        $stmt->bindParam(':id', $id);
        $stmt->execute();

        $row = $stmt->fetch(\PDO::FETCH_ASSOC);

        return ['name' => $row['name'], 'socket' => $row['socket']];
    }
}
