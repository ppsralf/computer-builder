<?php

namespace Data\Motherboard;

use Data\Manufacturer\Manufacturer;

class Motherboard implements \JsonSerializable
{
    private int $id;
    private string $name;
    private Manufacturer $manufacturer;
    private string $socket;
    private string $form_factor;
    private int $mem_max;
    private string $mem_type;
    private int $mem_slots;
    private int $sata_ports;
    private int $wifi;

    public function jsonSerialize()
    {
        return [
            'id' => $this->getID(),
            'name' => $this->getName(),
            'manufacturer' => $this->getManufacturerName(),
            'socket' => $this->getSocket(),
            'form_factor' => $this->getFormFactor(),
            'mem_max' => $this->getMemMax(),
            'mem_type' => $this->getMemType(),
            'mem_slots' => $this->getMemSlots(),
            'sata_ports' => $this->getSataPorts(),
            'wifi' => $this->getWifi()
        ];
    }

    public function __construct(string $name, Manufacturer $manufacturer, string $socket, string $form_factor, int $mem_max, string $mem_type, int $mem_slots, int $sata_ports, int $wifi)
    {
        $this->setName($name);
        $this->setManufacturer($manufacturer);
        $this->setSocket($socket);
        $this->setFormFactor($form_factor);
        $this->setMemMax($mem_max);
        $this->setMemType($mem_type);
        $this->setMemSlots($mem_slots);
        $this->setSataPorts($sata_ports);
        $this->setWifi($wifi);
    }

    public function setID(int $id)
    {
        $this->id = $id;
    }

    public function getID(): int
    {
        return $this->id;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setManufacturer(Manufacturer $manufacturer)
    {
        $this->manufacturer = $manufacturer;
    }

    public function getManufacturer(): Manufacturer
    {
        return $this->manufacturer;
    }

    public function getManufacturerName(): string
    {
        return $this->manufacturer->getName();
    }

    public function setSocket(string $socket)
    {
        $this->socket = $socket;
    }

    public function getSocket(): string
    {
        return $this->socket;
    }

    public function setFormFactor(string $form_factor)
    {
        $this->form_factor = $form_factor;
    }

    public function getFormFactor(): string
    {
        return $this->form_factor;
    }

    public function setMemMax(int $mem_max)
    {
        $this->mem_max = $mem_max;
    }

    public function getMemMax(): int
    {
        return $this->mem_max;
    }

    public function setMemType(string $mem_type)
    {
        $this->mem_type = $mem_type;
    }

    public function getMemType(): string
    {
        return $this->mem_type;
    }

    public function setMemSlots(int $mem_slots)
    {
        $this->mem_slots = $mem_slots;
    }

    public function getMemSlots(): int
    {
        return $this->mem_slots;
    }

    public function setSataPorts(int $sata_ports)
    {
        $this->sata_ports = $sata_ports;
    }

    public function getSataPorts(): int
    {
        return $this->sata_ports;
    }

    public function setWifi(int $wifi)
    {
        $this->wifi = $wifi;
    }

    public function getWifi(): int
    {
        return $this->wifi;
    }
}
