<?php

namespace Data\ComputerCase;

use \Data\Manufacturer\Manufacturer;

class ComputerCase implements \JsonSerializable
{
    private $id, $name, $manufacturer, $form_factor, $bay25, $bay35;

    public function jsonSerialize()
    {
        return [
            'id' => $this->getID(),
            'manufacturer' => $this->getManufacturerName(),
            'name' => $this->getName(),
            'form_factor' => $this->getFormFactor(),
            'bay25' => $this->getBay25(),
            'bay35' => $this->getBay35()
        ];
    }

    public function __construct($name, Manufacturer $manufacturer, $form_factor, $bay25, $bay35)
    {
        $this->setName($name);
        $this->setManufacturer($manufacturer);
        $this->setFormFactor($form_factor);
        $this->setBay25($bay25);
        $this->setBay35($bay35);
    }

    public function setID(int $id)
    {
        $this->id = $id;
    }

    public function getID(): int
    {
        return $this->id;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setManufacturer(Manufacturer $manufacturer)
    {
        $this->manufacturer = $manufacturer;
    }

    public function getManufacturer(): Manufacturer
    {
        return $this->manufacturer;
    }

    public function getManufacturerName(): string
    {
        return $this->manufacturer->getName();
    }

    public function setFormFactor(string $form_factor)
    {
        $this->form_factor = $form_factor;
    }

    public function getFormFactor(): string
    {
        return $this->form_factor;
    }

    public function setBay25(int $bay25)
    {
        $this->bay25 = $bay25;
    }

    public function getBay25(): int
    {
        return $this->bay25;
    }

    public function setBay35(int $bay35)
    {
        $this->bay35 = $bay35;
    }

    public function getBay35(): int
    {
        return $this->bay35;
    }
}
