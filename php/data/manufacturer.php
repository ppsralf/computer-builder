<?php

namespace Data\Manufacturer;

class Manufacturer implements \JsonSerializable
{
    private $id, $name;

    public function jsonSerialize()
    {
        return [
            'id' => $this->getID(),
            'name' => $this->getName()
        ];
    }

    public function __construct($name)
    {
        $this->setName($name);
    }

    public function setID(int $id)
    {
        $this->id = $id;
    }

    public function getID(): int
    {
        return $this->id;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
