<?php

namespace Data\PSU;

use Data\Manufacturer\Manufacturer;

class PSU implements \JsonSerializable
{
    private int $id;
    private string $name;
    private Manufacturer $manufacturer;
    private int $wattage;

    public function jsonSerialize()
    {
        return [
            'id' => $this->getID(),
            'name' => $this->getName(),
            'manufacturer' => $this->getManufacturerName(),
            'wattage' => $this->getWattage()
        ];
    }

    public function __construct(string $name, Manufacturer $manufacturer, int $wattage)
    {
        $this->setName($name);
        $this->setManufacturer($manufacturer);
        $this->setWattage($wattage);
    }

    public function setID(int $id)
    {
        $this->id = $id;
    }

    public function getID(): int
    {
        return $this->id;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setManufacturer(Manufacturer $manufacturer)
    {
        $this->manufacturer = $manufacturer;
    }

    public function getManufacturer(): Manufacturer
    {
        return $this->manufacturer;
    }

    public function getManufacturerName(): string
    {
        return $this->manufacturer->getName();
    }

    public function setWattage(int $wattage)
    {
        $this->wattage = $wattage;
    }

    public function getWattage(): int
    {
        return $this->wattage;
    }
}
