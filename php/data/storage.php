<?php

namespace Data\Storage;

use Data\Manufacturer\Manufacturer;

class Storage implements \JsonSerializable
{
    private int $id;
    private string $name;
    private Manufacturer $manufacturer;
    private int $capacity;
    private string $type;
    private string $form_factor;

    public function jsonSerialize()
    {
        return [
            'id' => $this->getID(),
            'name' => $this->getName(),
            'manufacturer' => $this->getManufacturerName(),
            'capacity' => $this->getCapacity(),
            'type' => $this->getType(),
            'form_factor' => $this->getFormFactor()
        ];
    }

    public function __construct(string $name, Manufacturer $manufacturer, int $capacity, string $type, string $form_factor)
    {
        $this->setName($name);
        $this->setManufacturer($manufacturer);
        $this->setCapacity($capacity);
        $this->setType($type);
        $this->setFormFactor($form_factor);
    }

    public function setID(int $id)
    {
        $this->id = $id;
    }

    public function getID(): int
    {
        return $this->id;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setManufacturer(Manufacturer $manufacturer)
    {
        $this->manufacturer = $manufacturer;
    }

    public function getManufacturer(): Manufacturer
    {
        return $this->manufacturer;
    }

    public function getManufacturerName(): string
    {
        return $this->manufacturer->getName();
    }

    public function setCapacity(int $capacity)
    {
        $this->capacity = $capacity;
    }

    public function getCapacity(): int
    {
        return $this->capacity;
    }

    public function setType(string $type)
    {
        $this->type = $type;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setFormFactor(string $form_factor)
    {
        $this->form_factor = $form_factor;
    }

    public function getFormFactor(): string
    {
        return $this->form_factor;
    }
}
