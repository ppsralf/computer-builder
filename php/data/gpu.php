<?php

namespace Data\GPU;

use \Data\Manufacturer\Manufacturer;

class GPU implements \JsonSerializable
{
    private $id, $name, $manufacturer, $chipset, $mem_type, $TDP;

    public function jsonSerialize()
    {
        return [
            'id' => $this->getID(),
            'name' => $this->getName(),
            'manufacturer' => $this->getManufacturerName(),
            'chipset' => $this->getChipset(),
            'mem_type' => $this->getMemType(),
            'TDP' => $this->getTDP()
        ];
    }

    public function __construct($name, Manufacturer $manufacturer, $chipset, $mem_type, $TDP)
    {
        $this->setName($name);
        $this->setManufacturer($manufacturer);
        $this->setChipset($chipset);
        $this->setMemType($mem_type);
        $this->setTDP($TDP);
    }

    public function setID(int $id)
    {
        $this->id = $id;
    }

    public function getID(): int
    {
        return $this->id;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setManufacturer(Manufacturer $manufacturer)
    {
        $this->manufacturer = $manufacturer;
    }

    public function getManufacturer(): Manufacturer
    {
        return $this->manufacturer;
    }

    public function getManufacturerName(): string
    {
        return $this->manufacturer->getName();
    }

    public function setChipset(string $chipset)
    {
        $this->chipset = $chipset;
    }

    public function getChipset(): string
    {
        return $this->chipset;
    }

    public function setMemType(string $mem_type)
    {
        $this->mem_type = $mem_type;
    }

    public function getMemType(): string
    {
        return $this->mem_type;
    }

    public function setTDP(string $TDP)
    {
        $this->TDP = $TDP;
    }

    public function getTDP(): string
    {
        return $this->TDP;
    }
}
