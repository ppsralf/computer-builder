<?php

namespace Data\Memory;

use Data\Manufacturer\Manufacturer;

class Memory implements \JsonSerializable
{
    private int $id;
    private string $name;
    private Manufacturer $manufacturer;
    private string $type;
    private int $modules;
    private int $size;

    public function jsonSerialize()
    {
        return [
            'id' => $this->getID(),
            'name' => $this->getName(),
            'manufacturer' => $this->getManufacturerName(),
            'type' => $this->getType(),
            'modules' => $this->getModules(),
            'size' => $this->getSize(),
        ];
    }

    public function __construct(string $name, Manufacturer $manufacturer, string $type, int $modules, int $size)
    {
        $this->setName($name);
        $this->setManufacturer($manufacturer);
        $this->setType($type);
        $this->setModules($modules);
        $this->setSize($size);
    }

    public function setID(int $id)
    {
        $this->id = $id;
    }

    public function getID(): int
    {
        return $this->id;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setManufacturer(Manufacturer $manufacturer)
    {
        $this->manufacturer = $manufacturer;
    }

    public function getManufacturer(): Manufacturer
    {
        return $this->manufacturer;
    }

    public function getManufacturerName(): string
    {
        return $this->manufacturer->getName();
    }

    public function setType(string $type)
    {
        $this->type = $type;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setModules(int $modules)
    {
        $this->modules = $modules;
    }

    public function getModules(): int
    {
        return $this->modules;
    }

    public function setSize(int $size)
    {
        $this->size = $size;
    }

    public function getSize(): int
    {
        return $this->size;
    }
}
