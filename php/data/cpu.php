<?php

namespace Data\CPU;

use \Data\Manufacturer\Manufacturer;

class CPU implements \JsonSerializable
{
    private $id, $name, $manufacturer, $core_count, $TDP, $socket, $graphics, $cooler;

    public function jsonSerialize()
    {
        return [
            'id' => $this->getID(),
            'name' => $this->getName(),
            'manufacturer' => $this->getManufacturerName(),
            'core_count' => $this->getCoreCount(),
            'TDP' => $this->getTDP(),
            'socket' => $this->getSocket(),
            'graphics' => $this->getGraphics(),
            'cooler' => $this->getCooler()
        ];
    }

    public function __construct($name, Manufacturer $manufacturer, $core_count, $TDP, $socket, $graphics, $cooler)
    {
        $this->setName($name);
        $this->setManufacturer($manufacturer);
        $this->setCoreCount($core_count);
        $this->setTDP($TDP);
        $this->setSocket($socket);
        $this->setGraphics($graphics);
        $this->setCooler($cooler);
    }

    public function setID(int $id)
    {
        $this->id = $id;
    }

    public function getID(): int
    {
        return $this->id;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setManufacturer(Manufacturer $manufacturer)
    {
        $this->manufacturer = $manufacturer;
    }

    public function getManufacturer(): Manufacturer
    {
        return $this->manufacturer;
    }

    public function getManufacturerName(): string
    {
        return $this->manufacturer->getName();
    }

    public function setCoreCount(int $core_count)
    {
        $this->core_count = $core_count;
    }

    public function getCoreCount(): int
    {
        return $this->core_count;
    }

    public function setTDP(int $TDP)
    {
        $this->TDP = $TDP;
    }

    public function getTDP(): int
    {
        return $this->TDP;
    }

    public function setSocket(string $socket)
    {
        $this->socket = $socket;
    }

    public function getSocket(): string
    {
        return $this->socket;
    }

    public function setGraphics(int $graphics)
    {
        $this->graphics = $graphics;
    }

    public function getGraphics(): int
    {
        return $this->graphics;
    }

    public function setCooler(int $cooler)
    {
        $this->cooler = $cooler;
    }

    public function getCooler(): int
    {
        return $this->cooler;
    }
}
