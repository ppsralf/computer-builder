<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');
header('Access-Control-Allow-Methods: GET, POST, PATCH, OPTIONS, DELETE');

error_reporting(E_ALL);
ini_set('display_errors', 'On');

require_once __DIR__ . "/DB/db.php";
require_once __DIR__ . "/services/DBServices.php";

$DBServices = new DBServices;

call_user_func([$DBServices, $_GET['func']]);
