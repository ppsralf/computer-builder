import React from "react";

function Loading() {
  return <div className="line-wobble"></div>;
}

export default Loading;
