import { createContext } from "react";

export const ComponentContext = createContext([
  {
    CPU: "",
    CPUW: 0,
    CPUS: "",
    MB: "",
    MBS: "",
    RAM: "",
    Storage: "",
    GPU: "",
    GPUW: 0,
    Case: "",
    PSU: "",
    PSUW: 0,
  },
]);
