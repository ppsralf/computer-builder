import React from "react";
import { useContext, useState, useEffect } from "react";
import { ComponentContext } from "../componentcontext";
import { useNavigate } from "react-router-dom";

function SelectStorage() {
  const { components, setComponents } = useContext(ComponentContext);
  const [data, setData] = useState([]);
  const [query, setQuery] = useState("");

  const navigate = useNavigate();

  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch(
        "http://localhost:8000/controller.php?func=fetchAllStorage"
      );
      const data = await response.json();
      setData(data);
    };
    fetchData();
  }, []);

  const filteredData = data.filter((item) => {
    return item.name.toLowerCase().includes(query.toLowerCase());
  });

  const handleButtonClick = (id) => {
    setComponents({ ...components, Storage: id });
    navigate("/");
  };

  return (
    (document.title = "Select Storage"),
    (
      <div className="select-container">
        <input
          type="text"
          className="select-container__search"
          placeholder="Search"
          value={query}
          onChange={(e) => setQuery(e.target.value)}
        />
        <div className="select-container__items-list">
          <table>
            <tr>
              <th>Manufacturer</th>
              <th>Name</th>
              <th>Capacity</th>
              <th>Type</th>
              <th>Form Factor</th>
            </tr>
            {filteredData.map((item) => (
              <tr key={item.id}>
                <th>{item.manufacturer}</th>
                <th>{item.name}</th>
                <th>{item.capacity}</th>
                <th>{item.type}</th>
                <th>{item.form_factor}</th>
                <input
                  type="submit"
                  className="select-container__item-submit"
                  value="+ Select"
                  onClick={() => handleButtonClick(item.id)}
                />
              </tr>
            ))}
          </table>
        </div>
      </div>
    )
  );
}

export default SelectStorage;
