import React from "react";
import { useContext, useState, useEffect } from "react";
import { ComponentContext } from "../componentcontext";
import { useNavigate } from "react-router-dom";

function SelectGPU() {
  const { components, setComponents } = useContext(ComponentContext);
  const [data, setData] = useState([]);
  const [query, setQuery] = useState("");

  const navigate = useNavigate();

  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch(
        "http://localhost:8000/controller.php?func=fetchAllGPUs"
      );
      const data = await response.json();
      setData(data);
    };
    fetchData();
  }, []);

  const filteredData = data.filter((item) => {
    return item.name.toLowerCase().includes(query.toLowerCase());
  });

  const handleButtonClick = (id) => {
    setComponents({ ...components, GPU: id });
    navigate("/");
  };

  return (
    (document.title = "Select GPU"),
    (
      <div className="select-container">
        <input
          type="text"
          className="select-container__search"
          placeholder="Search"
          value={query}
          onChange={(e) => setQuery(e.target.value)}
        />
        <div className="select-container__items-list">
          <table>
            <tr>
              <th>Manufacturer</th>
              <th>Name</th>
              <th>Chipset</th>
              <th>Memory Type</th>
              <th>TDP</th>
            </tr>
            {filteredData.map((item) => (
              <tr key={item.id}>
                <th>{item.manufacturer}</th>
                <th>{item.name}</th>
                <th>{item.chipset}</th>
                <th>{item.mem_type}</th>
                <th>{item.TDP} W</th>
                <input
                  type="submit"
                  className="select-container__item-submit"
                  value="+ Select"
                  onClick={() => handleButtonClick(item.id)}
                />
              </tr>
            ))}
          </table>
        </div>
      </div>
    )
  );
}

export default SelectGPU;
