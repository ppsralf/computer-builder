import React from "react";
import { useContext, useState, useEffect } from "react";
import { ComponentContext } from "../componentcontext";
import { useNavigate } from "react-router-dom";

function SelectCase() {
  const { components, setComponents } = useContext(ComponentContext);
  const [data, setData] = useState([]);
  const [query, setQuery] = useState("");

  const navigate = useNavigate();

  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch(
        "http://localhost:8000/controller.php?func=fetchAllComputerCases"
      );
      const data = await response.json();
      setData(data);
    };
    fetchData();
  }, []);

  const filteredData = data.filter((item) => {
    return item.name.toLowerCase().includes(query.toLowerCase());
  });

  const handleButtonClick = (id) => {
    setComponents({ ...components, Case: id });
    navigate("/");
  };

  return (
    (document.title = "Select Case"),
    (
      <div className="select-container">
        <input
          type="text"
          className="select-container__search"
          placeholder="Search"
          value={query}
          onChange={(e) => setQuery(e.target.value)}
        />
        <div className="select-container__items-list">
          <table>
            <tr>
              <th>Manufacturer</th>
              <th>Name</th>
              <th>Form Factor</th>
              <th>2.5 Bays</th>
              <th>3.5 Bays</th>
            </tr>
            {filteredData.map((item) => (
              <tr key={item.id}>
                <th>{item.manufacturer}</th>
                <th>{item.name}</th>
                <th>{item.form_factor}</th>
                <th>{item.bay25}</th>
                <th>{item.bay35}</th>
                <input
                  type="submit"
                  className="select-container__item-submit"
                  value="+ Select"
                  onClick={() => handleButtonClick(item.id)}
                />
              </tr>
            ))}
          </table>
        </div>
      </div>
    )
  );
}

export default SelectCase;
