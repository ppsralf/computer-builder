import React from "react";
import { useContext, useState, useEffect } from "react";
import { ComponentContext } from "../componentcontext";
import { useNavigate } from "react-router-dom";

function SelectMB() {
  const { components, setComponents } = useContext(ComponentContext);
  const [MBs, setMBs] = useState([]);
  const [query, setQuery] = useState("");

  const navigate = useNavigate();

  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch(
        "http://localhost:8000/controller.php?func=fetchAllMotherboards"
      );
      const data = await response.json();
      setMBs(data);
    };
    fetchData();
  }, []);

  const filteredMBs = MBs.filter((item) => {
    return item.name.toLowerCase().includes(query.toLowerCase());
  });

  const handleButtonClick = (id) => {
    setComponents({ ...components, MB: id });
    navigate("/");
  };

  return (
    (document.title = "Select CPU"),
    (
      <div className="select-container">
        <input
          type="text"
          className="select-container__search"
          placeholder="Search"
          value={query}
          onChange={(e) => setQuery(e.target.value)}
        />
        <div className="select-container__items-list">
          <table>
            <tr>
              <th>Manufacturer</th>
              <th>Name</th>
              <th>Socket</th>
              <th>Form Factor</th>
              <th>Max RAM (GB)</th>
              <th>Memory Type</th>
              <th>Memory Slots</th>
            </tr>
            {filteredMBs.map((item) => (
              <tr key={item.id}>
                <th>{item.manufacturer}</th>
                <th>{item.name}</th>
                <th>{item.socket}</th>
                <th>{item.form_factor}</th>
                <th>{item.mem_max}</th>
                <th>{item.mem_type}</th>
                <th>{item.mem_slots}</th>
                <input
                  type="submit"
                  className="select-container__item-submit"
                  value="+ Select"
                  onClick={() => handleButtonClick(item.id)}
                />
              </tr>
            ))}
          </table>
        </div>
      </div>
    )
  );
}

export default SelectMB;
