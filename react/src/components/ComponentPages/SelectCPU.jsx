import React from "react";
import { useContext, useState, useEffect } from "react";
import { ComponentContext } from "../componentcontext";
import { useNavigate } from "react-router-dom";

function SelectCPU() {
  const { components, setComponents } = useContext(ComponentContext);
  const [CPUs, setCPUs] = useState([]);
  const [query, setQuery] = useState("");

  const navigate = useNavigate();

  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch(
        "http://localhost:8000/controller.php?func=fetchAllCPUs"
      );
      const data = await response.json();
      console.log(data);
      setCPUs(data);
    };
    fetchData();
  }, []);

  const filteredCPUs = CPUs.filter((item) => {
    return item.name.toLowerCase().includes(query.toLowerCase());
  });

  const handleButtonClick = (id) => {
    setComponents({ ...components, CPU: id });
    navigate("/");
  };

  return (
    (document.title = "Select CPU"),
    (
      <div className="select-container">
        <input
          type="text"
          className="select-container__search"
          placeholder="Search"
          value={query}
          onChange={(e) => setQuery(e.target.value)}
        />
        <div className="select-container__items-list">
          <table>
            <tr>
              <th>Manufacturer</th>
              <th>Name</th>
              <th>Core Count</th>
              <th>Socket</th>
              <th>Integrated Graphics</th>
              <th>Included Cooler</th>
              <th>TDP</th>
            </tr>
            {filteredCPUs.map((item) => (
              <tr key={item.id}>
                <td>{item.manufacturer}</td>
                <td>{item.name}</td>
                <td>{item.core_count}</td>
                <td>{item.socket}</td>
                <td>{item.graphics}</td>
                <td> {item.cooler === 1 ? "Yes" : "No"}</td>
                <td>{item.TDP} W</td>
                <input
                  type="submit"
                  className="select-container__item-submit"
                  value="+ Select"
                  onClick={() => handleButtonClick(item.id)}
                />
              </tr>
            ))}
          </table>
        </div>
      </div>
    )
  );
}

export default SelectCPU;
