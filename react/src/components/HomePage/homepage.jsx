import React from "react";
import Title from "./title";
import Builder from "./builder";

function HomePage() {
  return (
    (document.title = "Computer Builder"),
    (
      // Atgriež JSX kodu, kas reprezentē lapas struktūru un sastāv no div elementiem
      <div id="home-page">
        <div className="title-container">
          <Title />
        </div>
        <Builder />
      </div>
    )
  );
}

export default HomePage;
