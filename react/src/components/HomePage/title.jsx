import React from "react";

function Title() {
  return (
    <div className="title">
      <span>Computer Builder</span>
    </div>
  );
}

export default Title;
