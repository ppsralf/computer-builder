import React from "react";
import { Link } from "react-router-dom";
import { useState, useContext, useEffect } from "react";
import { ComponentContext } from "../componentcontext";

function Builder() {
  // Iegūst komponentu stāvokļus un atjaunošanas funkcijas no ComponentContext
  const { components, setComponents } = useContext(ComponentContext);

  // Izveido stāvokļus un atjaunošanas funkcijas dažādiem komponentiem
  const [CPU, setCPU] = useState("");
  const [MB, setMB] = useState("");
  const [RAM, setRAM] = useState("");
  const [Storage, setStorage] = useState("");
  const [GPU, setGPU] = useState("");
  const [Case, setCase] = useState("");
  const [PSU, setPSU] = useState("");

  // Izveido stāvokli brīdinājumu ziņojumiem un kopējo jaudu
  const [warnings, setWarnings] = useState([]);
  const [wattage, setWattage] = useState(0);

  // Asinhrona funkcija, kas pārbauda komponentus
  async function checkComponents() {
    const component = { CPU, MB, RAM, Storage, GPU, Case, PSU };
    // Veic pieprasījumu uz serveri, lai pārbaudītu komponentus
    const response = await fetch(
      "http://localhost:8000/controller.php?func=checkComponents",
      {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(component),
      }
    );
    // Iegūst atbildi kā JSON datu formātu
    const data = await response.json();
    // Atjauno brīdinājumu ziņojumus un kopējo jaudu
    setWarnings(data.warnings);
    setWattage(data.totalWattage);
  }

  // Funkcija, kas notīra komponentu stāvokļus
  function clearComponents() {
    // Notīra visus komponentu stāvokļus un atjaunošanas funkcijas
    setCPU("");
    setMB("");
    setRAM("");
    setStorage("");
    setGPU("");
    setCase("");
    setPSU("");
    setWarnings([]);
    setWattage(0);

    // Notīra komponentu stāvokļus arī ComponentContext
    setComponents({
      CPU: "",
      CPUW: 0,
      CPUS: "",
      MB: "",
      MBS: "",
      RAM: "",
      Storage: "",
      GPU: "",
      GPUW: 0,
      Case: "",
      PSU: "",
      PSUW: 0,
    });
  }

  // Asinhrona funkcija, kas iegūst informāciju par CPU
  const fetchCPUInfo = async () => {
    // Veic pieprasījumu uz serveri, lai iegūtu CPU informāciju
    const response = await fetch(
      "http://localhost:8000/controller.php?func=getCPUInfo",
      {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: components.CPU,
      }
    );
    // Iegūst atbildi kā JSON datu formātu
    const data = await response.json();
    // Atjauno CPU stāvokli, noņemot pēdiņas no nosaukuma
    setCPU(data.name.replace(/['"]+/g, ""));
    // Atjauno CPU jaudu un ligzdas informāciju ComponentContext
    setComponents({ ...components, CPUW: data.wattage });
    setComponents({ ...components, CPUS: data.socket });
  };

  const fetchMBName = async () => {
    const response = await fetch(
      "http://localhost:8000/controller.php?func=getMBName",
      {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: components.MB,
      }
    );
    const data = await response.json();
    setMB(data.name.replace(/['"]+/g, ""));
    setComponents({ ...components, MBS: data.socket });
  };

  const fetchRAMName = async () => {
    const response = await fetch(
      "http://localhost:8000/controller.php?func=getMemoryName",
      {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: components.RAM,
      }
    );
    const data = await response.text();
    setRAM(data.replace(/['"]+/g, ""));
  };

  const fetchStorageInfo = async () => {
    const response = await fetch(
      "http://localhost:8000/controller.php?func=getStorageName",
      {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: components.Storage,
      }
    );
    const data = await response.text();
    setStorage(data.replace(/['"]+/g, ""));
  };

  const fetchGPUInfo = async () => {
    const response = await fetch(
      "http://localhost:8000/controller.php?func=getGPUInfo",
      {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: components.GPU,
      }
    );
    const data = await response.json();
    setGPU(data.name.replace(/['"]+/g, ""));
    setComponents({ ...components, GPUW: data.wattage });
  };

  const fetchCaseInfo = async () => {
    const response = await fetch(
      "http://localhost:8000/controller.php?func=getCCInfo",
      {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: components.Case,
      }
    );
    const data = await response.text();
    setCase(data.replace(/['"]+/g, ""));
  };

  const fetchPSUInfo = async () => {
    const response = await fetch(
      "http://localhost:8000/controller.php?func=getPSUName",
      {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: components.PSU,
      }
    );
    const data = await response.json();
    setPSU(data.name.replace(/['"]+/g, ""));
    setComponents({ ...components, PSUW: data.wattage });
  };

  useEffect(() => {
    fetchCPUInfo();
  }, [components.CPU]);

  useEffect(() => {
    fetchMBName();
  }, [components.MB]);

  useEffect(() => {
    fetchRAMName();
  }, [components.RAM]);

  useEffect(() => {
    fetchStorageInfo();
  }, [components.Storage]);

  useEffect(() => {
    fetchGPUInfo();
  }, [components.GPU]);

  useEffect(() => {
    fetchCaseInfo();
  }, [components.Case]);

  useEffect(() => {
    fetchPSUInfo();
  }, [components.PSU]);

  return (
    <div className="master-builder-container">
      <div className="builder-container">
        <div className="builder-container__title-component">
          <span className="builder-container-titles__component">Component</span>
        </div>
        <div className="builder-container__title-selection">
          <span className="builder-container-titles__selection">Selection</span>
        </div>
        <div className="builder-container__title-container">
          <span className="builder-container__title">CPU</span>
        </div>
        <div className="builder-container__selection">
          {CPU === "" ? (
            <Link to="/cpu" className="selection-button">
              <span className="selection-button__span">+ Select CPU</span>
            </Link>
          ) : (
            <Link to="/cpu" className="selection-button__selected">
              <span className="selection"> {CPU} </span>
            </Link>
          )}
        </div>
        <div className="builder-container__title-container">
          <span className="builder-container__title">Motherboard</span>
        </div>
        <div className="builder-container__selection">
          {MB === "" ? (
            <Link to="/mb" className="selection-button">
              <span className="selection-button__span">
                + Select Motherboard
              </span>
            </Link>
          ) : (
            <Link to="/mb" className="selection-button__selected">
              <span className="selection"> {MB} </span>
            </Link>
          )}
        </div>
        <div className="builder-container__title-container">
          <span className="builder-container__title">Memory</span>
        </div>
        <div className="builder-container__selection">
          {RAM === "" ? (
            <Link to="/ram" className="selection-button">
              <span className="selection-button__span">+ Select RAM</span>
            </Link>
          ) : (
            <Link to="/ram" className="selection-button__selected">
              <span className="selection"> {RAM} </span>
            </Link>
          )}
        </div>
        <div className="builder-container__title-container">
          <span className="builder-container__title">Storage</span>
        </div>
        <div className="builder-container__selection">
          {Storage === "" ? (
            <Link to="/storage" className="selection-button">
              <span className="selection-button__span">+ Select Storage</span>
            </Link>
          ) : (
            <Link to="/storage" className="selection-button__selected">
              <span className="selection"> {Storage} </span>
            </Link>
          )}
        </div>
        <div className="builder-container__title-container">
          <span className="builder-container__title">Video Card</span>
        </div>
        <div className="builder-container__selection">
          {GPU === "" ? (
            <Link to="/gpu" className="selection-button">
              <span className="selection-button__span">+ Select GPU</span>
            </Link>
          ) : (
            <Link to="/gpu" className="selection-button__selected">
              <span className="selection"> {GPU} </span>
            </Link>
          )}
        </div>
        <div className="builder-container__title-container">
          <span className="builder-container__title">Case</span>
        </div>
        <div className="builder-container__selection">
          {Case === "" ? (
            <Link to="/case" className="selection-button">
              <span className="selection-button__span">+ Select Case</span>
            </Link>
          ) : (
            <Link to="/case" className="selection-button__selected">
              <span className="selection"> {Case} </span>
            </Link>
          )}
        </div>
        <div className="builder-container__title-container">
          <span className="builder-container__title">Power Supply</span>
        </div>
        <div className="builder-container__selection">
          {PSU === "" ? (
            <Link to="/psu" className="selection-button">
              <span className="selection-button__span">+ Select PSU</span>
            </Link>
          ) : (
            <Link to="/psu" className="selection-button__selected">
              <span className="selection"> {PSU} </span>
            </Link>
          )}
        </div>
        <div className="checkComponents">
          <button
            disabled={(CPU === "", GPU === "", MB === "", PSU === "")}
            onClick={checkComponents}
          >
            Check Components
          </button>{" "}
          <br />
          <button onClick={clearComponents}>Clear Selection</button>
          <br></br>
          <span>Estimated Wattage: {wattage}</span>
        </div>
        <div className="warnings">
          {warnings.map((warning) => (
            <ul>
              <li>{warning}</li>
            </ul>
          ))}
        </div>
      </div>
    </div>
  );
}

export default Builder;
