import React from "react";
import { Link } from "react-router-dom";
import { useContext, useEffect } from "react";
import { useState } from "react";
import { UserContext } from "./usercontext";
import { useNavigate } from "react-router-dom";

function Navigation(props) {
  const user = useContext(UserContext);
  const navigate = useNavigate();
  const [toggle, setToggle] = useState(false);

  useEffect(() => {
    props.setLogOut(toggle);
  }, [toggle]);

  return (
    <div id="navigation">
      <div id="nav-buttons">
        <Link to="/" id="builder-button">
          <span id="builder-button__text">Builder</span>
        </Link>
        <Link to="/admin" id="login-button">
          <span id="login-button__text">{user ? "Admin Panel" : "Log In"}</span>
        </Link>
      </div>
      {user && (
        <div id="auth-user-text__container">
          <button id="auth-user__button" onClick={() => setToggle(!toggle)}>
            <div id="auth-user__button__icon">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="icon icon-tabler icon-tabler-logout"
                width="32"
                height="32"
                viewBox="0 0 24 24"
                strokeWidth="1.5"
                stroke="#FFFFFF"
                fill="none"
                strokeLinecap="round"
                strokeLinejoin="round"
              >
                <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                <path d="M14 8v-2a2 2 0 0 0 -2 -2h-7a2 2 0 0 0 -2 2v12a2 2 0 0 0 2 2h7a2 2 0 0 0 2 -2v-2" />
                <path d="M7 12h14l-3 -3m0 6l3 -3" />
              </svg>
            </div>
            <span id="auth-user__button__text">Log Out</span>
          </button>
          <span id="auth-user__text">
            <div id="auth-user__icon">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="icon icon-tabler icon-tabler-user-circle"
                width="32"
                height="32"
                viewBox="0 0 24 24"
                strokeWidth="1.5"
                stroke="#FFFFFF"
                fill="none"
                strokeLinecap="round"
                strokeLinejoin="round"
              >
                <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                <circle cx="12" cy="12" r="9" />
                <circle cx="12" cy="10" r="3" />
                <path d="M6.168 18.849a4 4 0 0 1 3.832 -2.849h4a4 4 0 0 1 3.834 2.855" />
              </svg>
            </div>
            <span id="auth-user__text__user">{user}</span>
          </span>
        </div>
      )}
    </div>
  );
}

export default Navigation;
