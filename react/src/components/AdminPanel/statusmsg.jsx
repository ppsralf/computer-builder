import React from "react";

function StatusMSG(props) {
  return <span className="bold-text">{props.statusMSG}</span>;
}

export default StatusMSG;
