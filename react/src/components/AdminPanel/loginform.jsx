import React from "react";
import { useState } from "react";
import Loading from "../loading";
import StatusMSG from "./statusmsg";

function LoginForm(props) {
  const [details, setDetails] = useState({ username: "", password: "" });

  const [displayStatusDiv, setDisplayStatusDiv] = useState(false);
  const [loading, setLoading] = useState(false);
  const [statusMSG, setStatusMSG] = useState("");

  const submitHandler = (e) => {
    e.preventDefault();
    setStatusMSG(null);
    setDisplayStatusDiv(true);
    setLoading(true);

    fetch("http://localhost:8000/controller.php?func=adminLogin", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(details),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result === true) {
          setDisplayStatusDiv(false);
          props.setAuthUser(details.username);
        } else {
          setLoading(false);
          setStatusMSG(result.status);
        }
      });
  };

  return (
    <form onSubmit={submitHandler}>
      <div className="inner-form-container">
        <input
          type="text"
          name="username"
          id="username"
          placeholder="Username"
          autoComplete="off"
          onChange={(e) => setDetails({ ...details, username: e.target.value })}
          value={details.username}
          required
        />
        <input
          type="password"
          name="password"
          id="password"
          placeholder="Password"
          autoComplete="off"
          onChange={(e) => setDetails({ ...details, password: e.target.value })}
          value={details.password}
          required
        />
        {displayStatusDiv && (
          <div id="insert-status">
            {statusMSG && <StatusMSG statusMSG={statusMSG} />}
            {loading && <Loading />}
          </div>
        )}
        <input type="submit" value="Log In" name="login" id="login" />
      </div>
    </form>
  );
}

export default LoginForm;
