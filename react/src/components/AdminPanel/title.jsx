import React from "react";

function Title() {
  return (
    <div className="title">
      <span>Admin Panel</span>
    </div>
  );
}

export default Title;
