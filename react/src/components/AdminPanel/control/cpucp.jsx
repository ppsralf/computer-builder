import { React, useState, useEffect } from "react";
import Loading from "../../loading";
import StatusMSG from "../statusmsg";

function CPUCP(props) {
  const [showForm, setShowForm] = useState("");

  const [CPUs, setCPUs] = useState([]);
  const [manufacturers, setManufacturers] = useState([]);
  const [cpuID, setCPUID] = useState("");

  const [name, setName] = useState("");
  const [manufacturer, setManufacturer] = useState("");
  const [coreCount, setCoreCount] = useState("");
  const [TDP, setTDP] = useState("");
  const [socket, setSocket] = useState("");
  const [iGraphics, setIGraphics] = useState("");
  const [cooler, setCooler] = useState("");

  const [loading, setLoading] = useState(false);
  const [statusMSG, setStatusMSG] = useState("CPU Managament");

  const fetchAllData = async () => {
    try {
      const response = await fetch(
        "http://localhost:8000/controller.php?func=fetchAllCPUs"
      );
      const newData = await response.json();
      if (!response.ok) {
        throw Error("Could not fetch data from resource.");
      }
      setCPUs(newData);
    } catch (err) {
      setStatusMSG(err.message);
    }
    try {
      const response = await fetch(
        "http://localhost:8000/controller.php?func=fetchAllManufacturers"
      );
      const newData = await response.json();
      if (!response.ok) {
        throw Error("Could not fetch data from resource.");
      }
      setManufacturers(newData);
    } catch (err) {
      setStatusMSG(err.message);
    }
  };

  const handleInsertSubmit = (e) => {
    e.preventDefault();
    setStatusMSG("");
    setLoading(true);

    const formData = {
      name,
      manufacturer,
      coreCount,
      TDP,
      socket,
      iGraphics,
      cooler,
    };

    fetch("http://localhost:8000/controller.php?func=insertCPU", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(formData),
    }).then((response) => {
      if (!response.ok) {
        // eslint-disable-next-line
        const data = response.json().then((data) => {
          setLoading(false);
          setStatusMSG("⚠️ " + data.status);
          setName("");
          setManufacturer("");
          setCoreCount("");
          setTDP("");
          setSocket("");
          setIGraphics("");
          setCooler("");
        });
        return false;
      }
      setLoading(false);
      setStatusMSG("✅ CPU Added");
      setName("");
      setManufacturer("");
      setCoreCount("");
      setTDP("");
      setSocket("");
      setIGraphics("");
      setCooler("");
    });
  };

  const handleDeleteSubmit = (e) => {
    e.preventDefault();
    setStatusMSG("");
    setLoading(true);

    if (cpuID === "") {
      setLoading(false);
      setStatusMSG("⚠️ No CPU Selected");
      setCPUID("");
      return false;
    }

    const formData = { cpuID };

    fetch("http://localhost:8000/controller.php?func=deleteCPU", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(formData),
    }).then((response) => {
      if (!response.ok) {
        // eslint-disable-next-line
        const data = response.json().then((data) => {
          setLoading(false);
          setStatusMSG("⚠️ " + data.status);
          setCPUID("");
        });
        return false;
      }
      setLoading(false);
      setStatusMSG("✅ CPU Deleted");
      setCPUID("");
    });
  };

  useEffect(() => {
    fetchAllData();
  }, [loading]);

  return (
    <div className="admin-menu__selection__container">
      {showForm === "" && (
        <div className="admin-menu__selection__form">
          <form>
            <input
              className="form-button"
              type="button"
              onClick={() => setShowForm("add")}
              value="Add"
            />
            <input
              className="form-button"
              type="button"
              onClick={() => setShowForm("delete")}
              value="Delete"
            />
          </form>
        </div>
      )}
      {showForm === "add" && (
        <div className="admin-menu__selection__form">
          <form onSubmit={handleInsertSubmit}>
            <select
              className="input-area__selection"
              name="user-select"
              id="user-select"
              onChange={(e) => setManufacturer(e.target.value)}
              value={manufacturer}
              required
            >
              <option value="" disabled>
                Select Manufacturer
              </option>
              {manufacturers.map((manufacturer) => (
                <option key={manufacturer.id}>{manufacturer.name}</option>
              ))}
            </select>
            <input
              type="text"
              name="name"
              className="input-area"
              placeholder="CPU name"
              autoComplete="off"
              onChange={(e) => setName(e.target.value)}
              value={name}
              maxLength={"50"}
              required
            />
            <input
              type="text"
              name="core-count"
              className="input-area"
              placeholder="Core Count"
              autoComplete="off"
              onChange={(e) => setCoreCount(e.target.value)}
              value={coreCount}
              maxLength={"2"}
              pattern="[0-9\/]*"
              required
            />
            <input
              type="text"
              name="core-count"
              className="input-area"
              placeholder="TDP (W)"
              autoComplete="off"
              onChange={(e) => setTDP(e.target.value)}
              value={TDP}
              maxLength={"3"}
              pattern="[0-9\/]*"
              required
            />
            <input
              type="text"
              name="socket"
              className="input-area"
              placeholder="Socket"
              autoComplete="off"
              onChange={(e) => setSocket(e.target.value)}
              value={socket}
              maxLength={"10"}
              required
            />
            <select
              name="iGraphics"
              className="input-area__selection"
              onChange={(e) => setIGraphics(e.target.value)}
              value={iGraphics}
            >
              <option value="" disabled>
                Has Integrated Graphics?
              </option>
              <option value="0">No Integrated Graphics</option>
              <option value="1">Has Integrated Graphics</option>
            </select>
            <select
              name="cooler"
              className="input-area__selection"
              onChange={(e) => setCooler(e.target.value)}
              value={cooler}
            >
              <option value="" disabled>
                Cooler Included?
              </option>
              <option value="0">No Cooler Included</option>
              <option value="1">Cooler Included</option>
            </select>
            <input
              type="submit"
              className="form-button"
              value="Add"
              name="add"
              id="add"
            />
          </form>
        </div>
      )}
      {showForm === "delete" && (
        <div className="admin-menu__selection__form">
          <form onSubmit={handleDeleteSubmit}>
            <select
              className="input-area__selection"
              name="cpu-select"
              id="cpu-select"
              onChange={(e) => setCPUID(e.target.value)}
              value={cpuID}
              required
            >
              <option value="" disabled>
                Select CPU
              </option>
              {CPUs.map((cpu) => (
                <option key={cpu.id} value={cpu.id}>
                  {cpu.name}
                </option>
              ))}
            </select>
            <input
              type="submit"
              className="form-button"
              value="Delete"
              name="delete"
              id="delete"
            />
          </form>
        </div>
      )}
      <div className="admin-menu__selection__msg">
        {loading && <Loading />}
        <StatusMSG statusMSG={statusMSG} />
      </div>
    </div>
  );
}

export default CPUCP;
