import React from "react";
import { useEffect, useState } from "react";

function AdminMenu(props) {
  const [selection, setSelection] = useState("users");

  useEffect(() => {
    props.setSelection(selection);
  }, [selection]);

  return (
    <div className="admin-control-panel-container">
      <div id="option-container">
        <div id="users-option__container" className="option-container__button">
          <button onClick={() => setSelection("users")}>Manage Users</button>
        </div>
        <div
          id="computercase-option__container"
          className="option-container__button"
        >
          <button onClick={() => setSelection("computercases")}>
            Manage Computer Cases
          </button>
        </div>
        <div id="cpu-option__container" className="option-container__button">
          <button onClick={() => setSelection("cpus")}>Manage CPUs</button>
        </div>
        <div id="gpu-option__container" className="option-container__button">
          <button onClick={() => setSelection("gpus")}>Manage GPUs</button>
        </div>
        <div
          id="manufacturer-option__container"
          className="option-container__button"
        >
          <button onClick={() => setSelection("manufacturers")}>
            Manage Manufacturers
          </button>
        </div>
        <div
          id="motherboard-option__container"
          className="option-container__button"
        >
          <button onClick={() => setSelection("motherboards")}>
            Manage Motherboards
          </button>
        </div>
        <div
          id="storage-option__container"
          className="option-container__button"
        >
          <button onClick={() => setSelection("storage")}>
            Manage Storage Drives
          </button>
        </div>
        <div id="ram-option__container" className="option-container__button">
          <button onClick={() => setSelection("ram")}>Manage RAM</button>
        </div>
        <div id="psu-option__container" className="option-container__button">
          <button onClick={() => setSelection("psus")}>Manage PSUs</button>
        </div>
      </div>
    </div>
  );
}

export default AdminMenu;
