import { React, useState, useEffect } from "react";
import Loading from "../../loading";
import StatusMSG from "../statusmsg";

function StorageCP() {
  const [showForm, setShowForm] = useState("");

  const [storages, setStorages] = useState([]);
  const [manufacturers, setManufacturers] = useState([]);
  const [storageID, setStorageID] = useState("");

  const [name, setName] = useState("");
  const [manufacturer, setManufacturer] = useState("");
  const [capacity, setCapacity] = useState("");
  const [type, setType] = useState("");
  const [formFactor, setFormFactor] = useState("");

  const [loading, setLoading] = useState(false);
  const [statusMSG, setStatusMSG] = useState("Storage Managament");

  const fetchAllData = async () => {
    try {
      const response = await fetch(
        "http://localhost:8000/controller.php?func=fetchAllStorage"
      );
      const newData = await response.json();
      if (!response.ok) {
        throw Error("Could not fetch data from resource.");
      }
      setStorages(newData);
    } catch (err) {
      setStatusMSG(err.message);
    }
    try {
      const response = await fetch(
        "http://localhost:8000/controller.php?func=fetchAllManufacturers"
      );
      const newData = await response.json();
      if (!response.ok) {
        throw Error("Could not fetch data from resource.");
      }
      setManufacturers(newData);
    } catch (err) {
      setStatusMSG(err.message);
    }
  };

  const handleInsertSubmit = (e) => {
    e.preventDefault();
    setStatusMSG("");
    setLoading(true);

    const formData = {
      name,
      manufacturer,
      capacity,
      type,
      formFactor,
    };

    fetch("http://localhost:8000/controller.php?func=insertStorage", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(formData),
    }).then((response) => {
      if (!response.ok) {
        // eslint-disable-next-line
        const data = response.json().then((data) => {
          setLoading(false);
          setStatusMSG("⚠️ " + data.status);
        });
        return false;
      }
      setLoading(false);
      setStatusMSG("✅ Storage Added");
      setName("");
      setManufacturer("");
      setCapacity("");
      setType("");
      setFormFactor("");
    });
  };

  const handleDeleteSubmit = (e) => {
    e.preventDefault();
    setStatusMSG("");
    setLoading(true);

    if (storageID === "") {
      setLoading(false);
      setStatusMSG("⚠️ No Storage Selected");
      setStorageID("");
      return false;
    }

    const formData = { storageID };

    fetch("http://localhost:8000/controller.php?func=deleteStorage", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(formData),
    }).then((response) => {
      if (!response.ok) {
        // eslint-disable-next-line
        const data = response.json().then((data) => {
          setLoading(false);
          setStatusMSG("⚠️ " + data.status);
          setStorageID("");
        });
        return false;
      }
      setLoading(false);
      setStatusMSG("✅ Storage Deleted");
      setStorageID("");
    });
  };

  useEffect(() => {
    fetchAllData();
  }, [loading]);

  return (
    <div className="admin-menu__selection__container">
      {showForm === "" && (
        <div className="admin-menu__selection__form">
          <form>
            <input
              className="form-button"
              type="button"
              onClick={() => setShowForm("add")}
              value="Add"
            />
            <input
              className="form-button"
              type="button"
              onClick={() => setShowForm("delete")}
              value="Delete"
            />
          </form>
        </div>
      )}
      {showForm === "add" && (
        <div className="admin-menu__selection__form">
          <form onSubmit={handleInsertSubmit}>
            <input
              type="text"
              value={name}
              className="input-area"
              placeholder="Name"
              autoComplete="off"
              onChange={(e) => setName(e.target.value)}
              maxLength={"30"}
              required
            />
            <select
              className="input-area__selection"
              name="user-select"
              id="user-select"
              onChange={(e) => setManufacturer(e.target.value)}
              value={manufacturer}
              required
            >
              <option value="" disabled>
                Select Manufacturer
              </option>
              {manufacturers.map((manufacturer) => (
                <option key={manufacturer.id}>{manufacturer.name}</option>
              ))}
            </select>
            <input
              type="number"
              value={capacity}
              className="input-area"
              placeholder="Capacity (GB)"
              autoComplete="off"
              onChange={(e) => setCapacity(e.target.value)}
              required
            />
            <select
              className="input-area__selection"
              onChange={(e) => setType(e.target.value)}
              value={type}
              required
            >
              <option value="" disabled>
                Storage Type
              </option>
              <option value="HDD">HDD</option>
              <option value="SSD">SSD</option>
            </select>
            <select
              className="input-area__selection"
              onChange={(e) => setFormFactor(e.target.value)}
              value={formFactor}
              required
            >
              <option value="" disabled>
                Form Factor
              </option>
              <option value="2.5">2.5</option>
              <option value="3.5">3.5</option>
            </select>
            <input type="submit" className="form-button" value="Add" />
          </form>
        </div>
      )}
      {showForm === "delete" && (
        <div className="admin-menu__selection__form">
          <form onSubmit={handleDeleteSubmit}>
            <select
              className="input-area__selection"
              onChange={(e) => setStorageID(e.target.value)}
              value={storageID}
              required
            >
              <option value="" disabled>
                Select Storage
              </option>
              {storages.map((st) => (
                <option key={st.id} value={st.id}>
                  {st.name}
                </option>
              ))}
            </select>
            <input
              type="submit"
              className="form-button"
              value="Delete"
              name="delete"
              id="delete"
            />
          </form>
        </div>
      )}
      <div className="admin-menu__selection__msg">
        {loading && <Loading />}
        <StatusMSG statusMSG={statusMSG} />
      </div>
    </div>
  );
}

export default StorageCP;
