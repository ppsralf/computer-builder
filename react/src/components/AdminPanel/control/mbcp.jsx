import { React, useState, useEffect } from "react";
import Loading from "../../loading";
import StatusMSG from "../statusmsg";

function MBCP() {
  const [showForm, setShowForm] = useState("");

  const [MBs, setMBs] = useState([]);
  const [manufacturers, setManufacturers] = useState([]);
  const [MBID, setMBID] = useState("");

  const [name, setName] = useState("");
  const [manufacturer, setManufacturer] = useState("");
  const [socket, setSocket] = useState("");
  const [formFactor, setFormFactor] = useState("");
  const [memMax, setMemMax] = useState("");
  const [memType, setMemType] = useState("");
  const [memSlots, setMemSlots] = useState("");
  const [sataPorts, setSataPorts] = useState("");
  const [wifi, setWifi] = useState("");

  const [loading, setLoading] = useState(false);
  const [statusMSG, setStatusMSG] = useState("Motherboard Managament");

  const fetchAllData = async () => {
    try {
      const response = await fetch(
        "http://localhost:8000/controller.php?func=fetchAllMotherboards"
      );
      const newData = await response.json();
      if (!response.ok) {
        throw Error("Could not fetch data from resource.");
      }
      setMBs(newData);
    } catch (err) {
      setStatusMSG(err.message);
    }
    try {
      const response = await fetch(
        "http://localhost:8000/controller.php?func=fetchAllManufacturers"
      );
      const newData = await response.json();
      if (!response.ok) {
        throw Error("Could not fetch data from resource.");
      }
      setManufacturers(newData);
    } catch (err) {
      setStatusMSG(err.message);
    }
  };

  const handleInsertSubmit = (e) => {
    e.preventDefault();
    setStatusMSG("");
    setLoading(true);

    const formData = {
      name,
      manufacturer,
      socket,
      formFactor,
      memMax,
      memType,
      memSlots,
      sataPorts,
      wifi,
    };

    fetch("http://localhost:8000/controller.php?func=insertMotherboard", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(formData),
    }).then((response) => {
      if (!response.ok) {
        // eslint-disable-next-line
        const data = response.json().then((data) => {
          setLoading(false);
          setStatusMSG("⚠️ " + data.status);
        });
        return false;
      }
      setLoading(false);
      setStatusMSG("✅ Motherboard Added");
      setName("");
      setManufacturer("");
      setSocket("");
      setFormFactor("");
      setMemMax("");
      setMemType("");
      setMemSlots("");
      setSataPorts("");
      setWifi("");
    });
  };

  const handleDeleteSubmit = (e) => {
    e.preventDefault();
    setStatusMSG("");
    setLoading(true);

    if (MBID === "") {
      setLoading(false);
      setStatusMSG("⚠️ No Motherboard Selected");
      setMBID("");
      return false;
    }

    const formData = { MBID };

    fetch("http://localhost:8000/controller.php?func=deleteMotherboard", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(formData),
    }).then((response) => {
      if (!response.ok) {
        // eslint-disable-next-line
        const data = response.json().then((data) => {
          setLoading(false);
          setStatusMSG("⚠️ " + data.status);
          setMBID("");
        });
        return false;
      }
      setLoading(false);
      setStatusMSG("✅ Motherboard Deleted");
      setMBID("");
    });
  };

  useEffect(() => {
    fetchAllData();
  }, [loading]);

  return (
    <div className="admin-menu__selection__container">
      {showForm === "" && (
        <div className="admin-menu__selection__form">
          <form>
            <input
              className="form-button"
              type="button"
              onClick={() => setShowForm("add")}
              value="Add"
            />
            <input
              className="form-button"
              type="button"
              onClick={() => setShowForm("delete")}
              value="Delete"
            />
          </form>
        </div>
      )}
      {showForm === "add" && (
        <div className="admin-menu__selection__form">
          <form onSubmit={handleInsertSubmit}>
            <input
              type="text"
              value={name}
              className="input-area"
              placeholder="Name"
              autoComplete="off"
              onChange={(e) => setName(e.target.value)}
              maxLength={"50"}
              required
            />
            <select
              className="input-area__selection"
              name="user-select"
              id="user-select"
              onChange={(e) => setManufacturer(e.target.value)}
              value={manufacturer}
              required
            >
              <option value="" disabled>
                Select Manufacturer
              </option>
              {manufacturers.map((manufacturer) => (
                <option key={manufacturer.id}>{manufacturer.name}</option>
              ))}
            </select>
            <input
              type="text"
              value={socket}
              className="input-area"
              placeholder="CPU Socket"
              autoComplete="off"
              onChange={(e) => setSocket(e.target.value)}
              maxLength={"20"}
              required
            />
            <select
              className="input-area__selection"
              onChange={(e) => setFormFactor(e.target.value)}
              value={formFactor}
              required
            >
              <option value="" disabled>
                Form Factor
              </option>
              <option value="ATX">ATX</option>
              <option value="EATX">EATX</option>
              <option value="Micro ATX">Micro ATX</option>
              <option value="Mini ITX">Mini ITX</option>
            </select>
            <input
              type="number"
              value={memMax}
              className="input-area"
              onChange={(e) => setMemMax(e.target.value)}
              required
              autoComplete="off"
              placeholder="Max RAM (GB)"
            />
            <select
              className="input-area__selection"
              onChange={(e) => setMemType(e.target.value)}
              value={memType}
              required
            >
              <option value="" disabled>
                Memory Type
              </option>
              <option value="DDR3">DDR3</option>
              <option value="DDR4">DDR4</option>
              <option value="DDR5">DDR5</option>
            </select>
            <input
              type="number"
              value={memSlots}
              className="input-area"
              onChange={(e) => setMemSlots(e.target.value)}
              required
              autoComplete="off"
              placeholder="Memory Slots"
            />
            <input
              type="number"
              value={sataPorts}
              className="input-area"
              onChange={(e) => setSataPorts(e.target.value)}
              required
              autoComplete="off"
              placeholder="Sata Ports"
            />
            <select
              className="input-area__selection"
              onChange={(e) => setWifi(e.target.value)}
              value={wifi}
              required
            >
              <option value="" disabled>
                Wifi
              </option>
              <option value="0">Doesn't have Wifi</option>
              <option value="1">Has Built-in Wifi</option>
            </select>
            <input type="submit" className="form-button" value="Add" />
          </form>
        </div>
      )}
      {showForm === "delete" && (
        <div className="admin-menu__selection__form">
          <form onSubmit={handleDeleteSubmit}>
            <select
              className="input-area__selection"
              onChange={(e) => setMBID(e.target.value)}
              value={MBID}
              required
            >
              <option value="" disabled>
                Select Motherboard
              </option>
              {MBs.map((mb) => (
                <option key={mb.id} value={mb.id}>
                  {mb.name}
                </option>
              ))}
            </select>
            <input
              type="submit"
              className="form-button"
              value="Delete"
              name="delete"
              id="delete"
            />
          </form>
        </div>
      )}
      <div className="admin-menu__selection__msg">
        {loading && <Loading />}
        <StatusMSG statusMSG={statusMSG} />
      </div>
    </div>
  );
}

export default MBCP;
