import { React, useState, useEffect } from "react";
import Loading from "../../loading";
import StatusMSG from "../statusmsg";

function ComputerCaseCP() {
  const [showForm, setShowForm] = useState("");

  const [CCs, setCCs] = useState([]);
  const [manufacturers, setManufacturers] = useState([]);
  const [CCID, setCCID] = useState("");

  const [name, setName] = useState("");
  const [manufacturer, setManufacturer] = useState("");
  const [formFactor, setFormFactor] = useState("");
  const [bay25, setBay25] = useState("");
  const [bay35, setBay35] = useState("");

  const [loading, setLoading] = useState(false);
  const [statusMSG, setStatusMSG] = useState("Computer Case Managament");

  const fetchAllData = async () => {
    try {
      const response = await fetch(
        "http://localhost:8000/controller.php?func=fetchAllComputerCases"
      );
      const newData = await response.json();
      if (!response.ok) {
        throw Error("Could not fetch data from resource.");
      }
      setCCs(newData);
    } catch (err) {
      setStatusMSG(err.message);
    }
    try {
      const response = await fetch(
        "http://localhost:8000/controller.php?func=fetchAllManufacturers"
      );
      const newData = await response.json();
      if (!response.ok) {
        throw Error("Could not fetch data from resource.");
      }
      setManufacturers(newData);
    } catch (err) {
      setStatusMSG(err.message);
    }
  };

  const handleInsertSubmit = (e) => {
    e.preventDefault();
    setStatusMSG("");
    setLoading(true);

    const formData = {
      name,
      manufacturer,
      formFactor,
      bay25,
      bay35,
    };

    fetch("http://localhost:8000/controller.php?func=insertComputerCase", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(formData),
    }).then((response) => {
      if (!response.ok) {
        // eslint-disable-next-line
        const data = response.json().then((data) => {
          setLoading(false);
          setStatusMSG("⚠️ " + data.status);
          setName("");
          setManufacturer("");
          setFormFactor("");
          setBay25("");
          setBay35("");
        });
        return false;
      }
      setLoading(false);
      setStatusMSG("✅ Computer Case Added");
      setName("");
      setManufacturer("");
      setFormFactor("");
      setBay25("");
      setBay35("");
    });
  };

  const handleDeleteSubmit = (e) => {
    e.preventDefault();
    setStatusMSG("");
    setLoading(true);

    if (CCID === "") {
      setLoading(false);
      setStatusMSG("⚠️ No Computer Case Selected");
      setCCID("");
      return false;
    }

    const formData = { CCID };

    fetch("http://localhost:8000/controller.php?func=deleteComputerCase", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(formData),
    }).then((response) => {
      if (!response.ok) {
        // eslint-disable-next-line
        const data = response.json().then((data) => {
          setLoading(false);
          setStatusMSG("⚠️ " + data.status);
          setCCID("");
        });
        return false;
      }
      setLoading(false);
      setStatusMSG("✅ Computer Case Deleted");
      setCCID("");
    });
  };

  useEffect(() => {
    fetchAllData();
  }, [loading]);

  return (
    <div className="admin-menu__selection__container">
      {showForm === "" && (
        <div className="admin-menu__selection__form">
          <form>
            <input
              className="form-button"
              type="button"
              onClick={() => setShowForm("add")}
              value="Add"
            />
            <input
              className="form-button"
              type="button"
              onClick={() => setShowForm("delete")}
              value="Delete"
            />
          </form>
        </div>
      )}
      {showForm === "add" && (
        <div className="admin-menu__selection__form">
          <form onSubmit={handleInsertSubmit}>
            <input
              type="text"
              value={name}
              className="input-area"
              placeholder="Case name"
              autoComplete="off"
              onChange={(e) => setName(e.target.value)}
              maxLength={"20"}
              required
            />
            <select
              className="input-area__selection"
              name="user-select"
              id="user-select"
              onChange={(e) => setManufacturer(e.target.value)}
              value={manufacturer}
              required
            >
              <option value="" disabled>
                Select Manufacturer
              </option>
              {manufacturers.map((manufacturer) => (
                <option key={manufacturer.id}>{manufacturer.name}</option>
              ))}
            </select>
            <select
              className="input-area__selection"
              onChange={(e) => setFormFactor(e.target.value)}
              value={formFactor}
              required
            >
              <option value="" disabled>
                Form Factor
              </option>
              <option value="ATX">ATX</option>
              <option value="EATX">EATX</option>
              <option value="Micro ATX">Micro ATX</option>
              <option value="Mini ITX">Mini ITX</option>
            </select>
            <input
              type="number"
              value={bay25}
              className="input-area"
              onChange={(e) => setBay25(e.target.value)}
              required
              autoComplete="off"
              placeholder="2.5 bay count"
            />
            <input
              type="number"
              value={bay35}
              className="input-area"
              onChange={(e) => setBay35(e.target.value)}
              required
              autoComplete="off"
              placeholder="3.5 bay count"
            />
            <input type="submit" className="form-button" value="Add" />
          </form>
        </div>
      )}
      {showForm === "delete" && (
        <div className="admin-menu__selection__form">
          <form onSubmit={handleDeleteSubmit}>
            <select
              className="input-area__selection"
              onChange={(e) => setCCID(e.target.value)}
              value={CCID}
              required
            >
              <option value="" disabled>
                Select Computer Case
              </option>
              {CCs.map((cc) => (
                <option key={cc.id} value={cc.id}>
                  {cc.name}
                </option>
              ))}
            </select>
            <input
              type="submit"
              className="form-button"
              value="Delete"
              name="delete"
              id="delete"
            />
          </form>
        </div>
      )}
      <div className="admin-menu__selection__msg">
        {loading && <Loading />}
        <StatusMSG statusMSG={statusMSG} />
      </div>
    </div>
  );
}

export default ComputerCaseCP;
