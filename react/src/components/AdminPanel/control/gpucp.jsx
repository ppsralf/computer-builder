import { React, useState, useEffect } from "react";
import Loading from "../../loading";
import StatusMSG from "../statusmsg";

function GPUCP() {
  const [showForm, setShowForm] = useState("");

  const [GPUs, setGPUs] = useState([]);
  const [manufacturers, setManufacturers] = useState([]);
  const [GPUID, setGPUID] = useState("");

  const [name, setName] = useState("");
  const [manufacturer, setManufacturer] = useState("");
  const [chipset, setChipset] = useState("");
  const [memType, setMemType] = useState("");
  const [TDP, setTDP] = useState("");

  const [loading, setLoading] = useState(false);
  const [statusMSG, setStatusMSG] = useState("GPU Managament");

  const fetchAllData = async () => {
    try {
      const response = await fetch(
        "http://localhost:8000/controller.php?func=fetchAllGPUs"
      );
      const newData = await response.json();
      if (!response.ok) {
        throw Error("Could not fetch data from resource.");
      }
      setGPUs(newData);
    } catch (err) {
      setStatusMSG(err.message);
    }
    try {
      const response = await fetch(
        "http://localhost:8000/controller.php?func=fetchAllManufacturers"
      );
      const newData = await response.json();
      if (!response.ok) {
        throw Error("Could not fetch data from resource.");
      }
      setManufacturers(newData);
    } catch (err) {
      setStatusMSG(err.message);
    }
  };

  const handleInsertSubmit = (e) => {
    e.preventDefault();
    setStatusMSG("");
    setLoading(true);

    const formData = {
      name,
      manufacturer,
      chipset,
      memType,
      TDP,
    };

    fetch("http://localhost:8000/controller.php?func=insertGPU", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(formData),
    }).then((response) => {
      if (!response.ok) {
        // eslint-disable-next-line
        const data = response.json().then((data) => {
          setLoading(false);
          setStatusMSG("⚠️ " + data.status);
          setName("");
          setManufacturer("");
          setChipset("");
          setMemType("");
          setTDP("");
        });
        return false;
      }
      setLoading(false);
      setStatusMSG("✅ GPU Added");
      setName("");
      setManufacturer("");
      setChipset("");
      setMemType("");
      setTDP("");
    });
  };

  const handleDeleteSubmit = (e) => {
    e.preventDefault();
    setStatusMSG("");
    setLoading(true);

    if (GPUID === "") {
      setLoading(false);
      setStatusMSG("⚠️ No Computer Case Selected");
      setGPUID("");
      return false;
    }

    const formData = { GPUID };

    fetch("http://localhost:8000/controller.php?func=deleteGPU", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(formData),
    }).then((response) => {
      if (!response.ok) {
        // eslint-disable-next-line
        const data = response.json().then((data) => {
          setLoading(false);
          setStatusMSG("⚠️ " + data.status);
          setGPUID("");
        });
        return false;
      }
      setLoading(false);
      setStatusMSG("✅ GPU Deleted");
      setGPUID("");
    });
  };

  useEffect(() => {
    fetchAllData();
  }, [loading]);

  return (
    <div className="admin-menu__selection__container">
      {showForm === "" && (
        <div className="admin-menu__selection__form">
          <form>
            <input
              className="form-button"
              type="button"
              onClick={() => setShowForm("add")}
              value="Add"
            />
            <input
              className="form-button"
              type="button"
              onClick={() => setShowForm("delete")}
              value="Delete"
            />
          </form>
        </div>
      )}
      {showForm === "add" && (
        <div className="admin-menu__selection__form">
          <form onSubmit={handleInsertSubmit}>
            <input
              type="text"
              name="name"
              className="input-area"
              placeholder="GPU name"
              autoComplete="off"
              onChange={(e) => setName(e.target.value)}
              value={name}
              maxLength={"50"}
              required
            />
            <select
              className="input-area__selection"
              name="user-select"
              id="user-select"
              onChange={(e) => setManufacturer(e.target.value)}
              value={manufacturer}
              required
            >
              <option value="" disabled>
                Select Manufacturer
              </option>
              {manufacturers.map((manufacturer) => (
                <option key={manufacturer.id}>{manufacturer.name}</option>
              ))}
            </select>
            <input
              type="text"
              name="name"
              className="input-area"
              placeholder="Chipset"
              autoComplete="off"
              onChange={(e) => setChipset(e.target.value)}
              value={chipset}
              maxLength={"30"}
              required
            />
            <select
              className="input-area__selection"
              onChange={(e) => setMemType(e.target.value)}
              value={memType}
              required
            >
              <option value="" disabled>
                Select Memory Type
              </option>
              <option value="GDDR5">GDDR5</option>
              <option value="GDDR5X">GDDR5X</option>
              <option value="GDDR6">GDDR6</option>
              <option value="GDDR6X">GDDR6X</option>
            </select>
            <input
              type="number"
              className="input-area"
              placeholder="TDP (W)"
              autoComplete="off"
              onChange={(e) => setTDP(e.target.value)}
              value={TDP}
              required
            />
            <input type="submit" className="form-button" value="Add" />
          </form>
        </div>
      )}
      {showForm === "delete" && (
        <div className="admin-menu__selection__form">
          <form onSubmit={handleDeleteSubmit}>
            <select
              className="input-area__selection"
              onChange={(e) => setGPUID(e.target.value)}
              value={GPUID}
              required
            >
              <option value="" disabled>
                Select GPU
              </option>
              {GPUs.map((gpu) => (
                <option key={gpu.id} value={gpu.id}>
                  {gpu.name}
                </option>
              ))}
            </select>
            <input
              type="submit"
              className="form-button"
              value="Delete"
              name="delete"
              id="delete"
            />
          </form>
        </div>
      )}
      <div className="admin-menu__selection__msg">
        {loading && <Loading />}
        <StatusMSG statusMSG={statusMSG} />
      </div>
    </div>
  );
}

export default GPUCP;
