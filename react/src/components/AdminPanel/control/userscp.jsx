import React from "react";
import { useState, useEffect, useContext } from "react";
import Loading from "../../loading";
import StatusMSG from "../statusmsg";
import { UserContext } from "../../usercontext";

function UsersCP() {
  const [authUser, setAuthUser] = useState(useContext(UserContext));
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [users, setUsers] = useState([]);
  const user = useContext(UserContext);
  const [selectedUser, setSelectedUser] = useState("");

  const [loading, setLoading] = useState(false);
  const [statusMSG, setStatusMSG] = useState("User Managament");

  const fetchUsers = async () => {
    try {
      const response = await fetch(
        "http://localhost:8000/controller.php?func=fetchAllUsers"
      );
      const newData = await response.json();
      if (!response.ok) {
        throw Error("Could not fetch data from resource.");
      }
      setUsers(newData);
    } catch (err) {
      setStatusMSG(err.message);
    }
  };

  const handleInsertSubmit = (e) => {
    e.preventDefault();
    setStatusMSG("");
    setLoading(true);

    if (authUser !== "admin") {
      setLoading(false);
      setStatusMSG("⚠️ Users can only be managed by Admin");
      return false;
    }

    if (username === password) {
      setLoading(false);
      setStatusMSG("⚠️ Password can not be the same as Username");
      return false;
    }

    const formData = { username, password };

    fetch("http://localhost:8000/controller.php?func=insertUser", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(formData),
    }).then((response) => {
      if (!response.ok) {
        // eslint-disable-next-line
        const data = response.json().then((data) => {
          setLoading(false);
          setStatusMSG("⚠️ " + data.status);
          setUsername("");
          setPassword("");
        });
        return false;
      }
      setLoading(false);
      setStatusMSG("✅ User Created");
      setUsername("");
      setPassword("");
    });
  };

  const handleDeleteSubmit = (e) => {
    e.preventDefault();
    setStatusMSG("");
    setLoading(true);

    if (authUser !== "admin") {
      setLoading(false);
      setStatusMSG("⚠️ Users can only be managed by Admin");
      return false;
    }

    if (selectedUser === "") {
      setLoading(false);
      setStatusMSG("⚠️ No User Selected");
      setSelectedUser("");
      return false;
    }

    if (selectedUser === "admin") {
      setLoading(false);
      setStatusMSG("⚠️ Admin user can not be deleted");
      setSelectedUser("");
      return false;
    }

    if (user === selectedUser) {
      setLoading(false);
      setStatusMSG("⚠️ User is currently authenticated");
      setSelectedUser("");
      return false;
    }

    const formData = { selectedUser };

    fetch("http://localhost:8000/controller.php?func=deleteUser", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(formData),
    }).then((response) => {
      if (!response.ok) {
        // eslint-disable-next-line
        const data = response.json().then((data) => {
          setLoading(false);
          setStatusMSG("⚠️ " + data.status + ".");
          setSelectedUser("");
        });
        return false;
      }
      setLoading(false);
      setStatusMSG("✅ User Deleted");
      setSelectedUser("");
    });
  };

  useEffect(() => {
    fetchUsers();
  }, [loading]);

  return (
    <div className="admin-menu__selection__container">
      <div className="admin-menu__selection__form">
        <form onSubmit={handleInsertSubmit}>
          <input
            type="text"
            name="username"
            id="username"
            className="input-area"
            placeholder="Username"
            autoComplete="off"
            onChange={(e) => setUsername(e.target.value.toLocaleLowerCase())}
            value={username}
            pattern="^[^0-9]\w+$"
            minLength={"3"}
            maxLength={"15"}
            required
          />
          <input
            type="password"
            name="password"
            id="password"
            className="input-area"
            placeholder="Password"
            autoComplete="off"
            onChange={(e) => setPassword(e.target.value)}
            value={password}
            minLength={"5"}
            maxLength={"30"}
            required
          />
          <input
            type="submit"
            className="form-button"
            value="Create User"
            name="createuser"
            id="createuser"
          />
        </form>
      </div>
      <div className="admin-menu__selection__form">
        <form onSubmit={handleDeleteSubmit}>
          <select
            className="input-area__selection"
            name="user-select"
            id="user-select"
            onChange={(e) => setSelectedUser(e.target.value)}
            value={selectedUser}
            required
          >
            <option value="" disabled>
              Select User
            </option>
            {users.map((user) => (
              <option key={user} value={user}>
                {user}
              </option>
            ))}
          </select>
          <input
            type="submit"
            className="form-button"
            value="Delete User"
            name="deleteuser"
            id="deleteuser"
          />
        </form>
      </div>
      <div className="admin-menu__selection__msg">
        {loading && <Loading />}
        <StatusMSG statusMSG={statusMSG} />
      </div>
    </div>
  );
}

export default UsersCP;
