import React from "react";
import { useState, useEffect } from "react";
import Loading from "../../loading";
import StatusMSG from "../statusmsg";

function ManufacturersCP() {
  const [name, setName] = useState("");
  const [manufacturers, setManufacturers] = useState([]);
  const [selected, setSelected] = useState("");

  const [loading, setLoading] = useState(false);
  const [statusMSG, setStatusMSG] = useState("Manufacturer Managament");

  const fetchAllData = async () => {
    try {
      const response = await fetch(
        "http://localhost:8000/controller.php?func=fetchAllManufacturers"
      );
      const newData = await response.json();
      if (!response.ok) {
        throw Error("Could not fetch data from resource.");
      }
      setManufacturers(newData);
    } catch (err) {
      setStatusMSG(err.message);
    }
  };

  const handleInsertSubmit = (e) => {
    e.preventDefault();
    setStatusMSG("");
    setLoading(true);

    const formData = { name };

    fetch("http://localhost:8000/controller.php?func=insertManufacturer", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(formData),
    }).then((response) => {
      if (!response.ok) {
        // eslint-disable-next-line
        const data = response.json().then((data) => {
          setLoading(false);
          setStatusMSG("⚠️ " + data.status);
          setName("");
        });
        return false;
      }
      setLoading(false);
      setStatusMSG("✅ Manufacturer Added");
      setName("");
    });
  };

  const handleDeleteSubmit = (e) => {
    e.preventDefault();
    setStatusMSG("");
    setLoading(true);

    if (selected === "") {
      setLoading(false);
      setStatusMSG("⚠️ No Manufacturer Selected");
      setSelected("");
      return false;
    }

    const formData = { selected };

    fetch("http://localhost:8000/controller.php?func=deleteManufacturer", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(formData),
    }).then((response) => {
      if (!response.ok) {
        // eslint-disable-next-line
        const data = response.json().then((data) => {
          setLoading(false);
          setStatusMSG("⚠️ " + data.status);
          setSelected("");
        });
        return false;
      }
      setLoading(false);
      setStatusMSG("✅ Manufacturer Deleted");
      setSelected("");
    });
  };

  useEffect(() => {
    fetchAllData();
  }, [loading]);

  return (
    <div className="admin-menu__selection__container">
      <div className="admin-menu__selection__form">
        <form onSubmit={handleInsertSubmit}>
          <input
            type="text"
            name="name"
            id="name"
            className="input-area"
            placeholder="Manufacturer Name"
            autoComplete="off"
            onChange={(e) => setName(e.target.value)}
            value={name}
            maxLength={"50"}
            required
          />
          <input
            type="submit"
            className="form-button"
            value="Add"
            name="add"
            id="add"
          />
        </form>
      </div>
      <div className="admin-menu__selection__form">
        <form onSubmit={handleDeleteSubmit}>
          <select
            className="input-area__selection"
            name="user-select"
            id="user-select"
            onChange={(e) => setSelected(e.target.value)}
            value={selected}
            required
          >
            <option value="" disabled>
              Select Manufacturer
            </option>
            {manufacturers.map((manufacturer) => (
              <option key={manufacturer.id}>{manufacturer.name}</option>
            ))}
          </select>
          <input
            type="submit"
            className="form-button"
            value="Delete"
            name="delete"
            id="delete"
          />
        </form>
      </div>
      <div className="admin-menu__selection__msg">
        {loading && <Loading />}
        <StatusMSG statusMSG={statusMSG} />
      </div>
    </div>
  );
}

export default ManufacturersCP;
