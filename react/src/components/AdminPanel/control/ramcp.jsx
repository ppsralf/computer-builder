import { React, useState, useEffect } from "react";
import Loading from "../../loading";
import StatusMSG from "../statusmsg";

function RAMCP() {
  const [showForm, setShowForm] = useState("");

  const [RAMs, setRAMs] = useState([]);
  const [manufacturers, setManufacturers] = useState([]);
  const [memID, setMemID] = useState("");

  const [name, setName] = useState("");
  const [manufacturer, setManufacturer] = useState("");
  const [type, setType] = useState("");
  const [modules, setModules] = useState("");
  const [size, setSize] = useState("");

  const [loading, setLoading] = useState(false);
  const [statusMSG, setStatusMSG] = useState("Memory Managament");

  const fetchAllData = async () => {
    try {
      const response = await fetch(
        "http://localhost:8000/controller.php?func=fetchAllMemory"
      );
      const newData = await response.json();
      if (!response.ok) {
        throw Error("Could not fetch data from resource.");
      }
      setRAMs(newData);
    } catch (err) {
      setStatusMSG(err.message);
    }
    try {
      const response = await fetch(
        "http://localhost:8000/controller.php?func=fetchAllManufacturers"
      );
      const newData = await response.json();
      if (!response.ok) {
        throw Error("Could not fetch data from resource.");
      }
      setManufacturers(newData);
    } catch (err) {
      setStatusMSG(err.message);
    }
  };

  const handleInsertSubmit = (e) => {
    e.preventDefault();
    setStatusMSG("");
    setLoading(true);

    const formData = {
      name,
      manufacturer,
      type,
      modules,
      size,
    };

    fetch("http://localhost:8000/controller.php?func=insertMemory", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(formData),
    }).then((response) => {
      if (!response.ok) {
        // eslint-disable-next-line
        const data = response.json().then((data) => {
          setLoading(false);
          setStatusMSG("⚠️ " + data.status);
        });
        return false;
      }
      setLoading(false);
      setStatusMSG("✅ Memory Added");
      setName("");
      setManufacturer("");
      setType("");
      setModules("");
      setSize("");
    });
  };

  const handleDeleteSubmit = (e) => {
    e.preventDefault();
    setStatusMSG("");
    setLoading(true);

    if (memID === "") {
      setLoading(false);
      setStatusMSG("⚠️ No Memory Selected");
      setMemID("");
      return false;
    }

    const formData = { memID };

    fetch("http://localhost:8000/controller.php?func=deleteMemory", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(formData),
    }).then((response) => {
      if (!response.ok) {
        // eslint-disable-next-line
        const data = response.json().then((data) => {
          setLoading(false);
          setStatusMSG("⚠️ " + data.status);
          setMemID("");
        });
        return false;
      }
      setLoading(false);
      setStatusMSG("✅ Memory Deleted");
      setMemID("");
    });
  };

  useEffect(() => {
    fetchAllData();
  }, [loading]);

  return (
    <div className="admin-menu__selection__container">
      {showForm === "" && (
        <div className="admin-menu__selection__form">
          <form>
            <input
              className="form-button"
              type="button"
              onClick={() => setShowForm("add")}
              value="Add"
            />
            <input
              className="form-button"
              type="button"
              onClick={() => setShowForm("delete")}
              value="Delete"
            />
          </form>
        </div>
      )}
      {showForm === "add" && (
        <div className="admin-menu__selection__form">
          <form onSubmit={handleInsertSubmit}>
            <input
              type="text"
              value={name}
              className="input-area"
              placeholder="Name"
              autoComplete="off"
              onChange={(e) => setName(e.target.value)}
              maxLength={"100"}
              required
            />
            <select
              className="input-area__selection"
              name="user-select"
              id="user-select"
              onChange={(e) => setManufacturer(e.target.value)}
              value={manufacturer}
              required
            >
              <option value="" disabled>
                Select Manufacturer
              </option>
              {manufacturers.map((manufacturer) => (
                <option key={manufacturer.id}>{manufacturer.name}</option>
              ))}
            </select>
            <select
              className="input-area__selection"
              onChange={(e) => setType(e.target.value)}
              value={type}
              required
            >
              <option value="" disabled>
                Type
              </option>
              <option value="DDR3">DDR3</option>
              <option value="DDR4">DDR4</option>
              <option value="DDR5">DDR5</option>
            </select>
            <input
              type="number"
              value={modules}
              className="input-area"
              onChange={(e) => setModules(e.target.value)}
              required
              autoComplete="off"
              placeholder="Module Count"
            />
            <input
              type="number"
              value={size}
              className="input-area"
              onChange={(e) => setSize(e.target.value)}
              required
              autoComplete="off"
              placeholder="Size (GB)"
            />
            <input type="submit" className="form-button" value="Add" />
          </form>
        </div>
      )}
      {showForm === "delete" && (
        <div className="admin-menu__selection__form">
          <form onSubmit={handleDeleteSubmit}>
            <select
              className="input-area__selection"
              onChange={(e) => setMemID(e.target.value)}
              value={memID}
              required
            >
              <option value="" disabled>
                Select Memory
              </option>
              {RAMs.map((mem) => (
                <option key={mem.id} value={mem.id}>
                  {mem.name}
                </option>
              ))}
            </select>
            <input
              type="submit"
              className="form-button"
              value="Delete"
              name="delete"
              id="delete"
            />
          </form>
        </div>
      )}
      <div className="admin-menu__selection__msg">
        {loading && <Loading />}
        <StatusMSG statusMSG={statusMSG} />
      </div>
    </div>
  );
}

export default RAMCP;
