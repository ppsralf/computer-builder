import { useEffect, useState } from "react";
import Title from "./title";
import LoginForm from "./loginform";
import { UserContext } from "../usercontext";
import { useContext } from "react";
import AdminMenu from "./control/adminmenu";
import UsersCP from "./control/userscp";
import ComputerCaseCP from "./control/computercasecp";
import CPUCP from "./control/cpucp";
import GPUCP from "./control/gpucp";
import ManufacturersCP from "./control/manufacturerscp";
import MBCP from "./control/mbcp";
import PSUCP from "./control/psucp";
import RAMCP from "./control/ramcp";
import StorageCP from "./control/storagecp";

function AdminPanel(props) {
  const [authUser, setAuthUser] = useState(useContext(UserContext));
  const [selection, setSelection] = useState("");

  useEffect(() => {
    props.setAuthUser(authUser);
  }, [authUser]);

  return (
    (document.title = "Admin Panel"),
    (
      <div id="admin-panel">
        <div className="title-container">
          <Title />
        </div>
        {!authUser ? (
          <div className="login-form-container">
            <LoginForm setAuthUser={setAuthUser} />
          </div>
        ) : (
          <div className="admin-menu-container">
            <div className="admin-menu">
              <AdminMenu setSelection={setSelection} />
            </div>
            <div className="admin-menu__selection">
              {selection === "users" && <UsersCP />}
              {selection === "computercases" && <ComputerCaseCP />}
              {selection === "cpus" && <CPUCP />}
              {selection === "gpus" && <GPUCP />}
              {selection === "manufacturers" && <ManufacturersCP />}
              {selection === "motherboards" && <MBCP />}
              {selection === "ram" && <RAMCP />}
              {selection === "psus" && <PSUCP />}
              {selection === "storage" && <StorageCP />}
            </div>
          </div>
        )}
      </div>
    )
  );
}

export default AdminPanel;
