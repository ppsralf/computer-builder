import { Routes, Route, useNavigate } from "react-router-dom";
import HomePage from "./components/HomePage/homepage";
import AdminPanel from "./components/AdminPanel/adminpanel";
import Navigation from "./components/navigation";
import SelectCPU from "./components/ComponentPages/SelectCPU";
import SelectMB from "./components/ComponentPages/SelectMB";
import SelectRAM from "./components/ComponentPages/SelectRAM";
import SelectStorage from "./components/ComponentPages/SelectStorage";
import SelectGPU from "./components/ComponentPages/SelectGPU";
import SelectCase from "./components/ComponentPages/SelectCase";
import SelectPSU from "./components/ComponentPages/SelectPSU";
import { useState, useEffect } from "react";
import { useContext } from "react";
import { UserContext } from "./components/usercontext";
import { ComponentContext } from "./components/componentcontext";

function App() {
  const [authUser, setAuthUser] = useState(useContext(UserContext));
  const [components, setComponents] = useState({
    CPU: "",
    MB: "",
    RAM: "",
    Storage: "",
    GPU: "",
    Case: "",
    PSU: "",
  });
  const [logout, setLogOut] = useState(null);
  const navigate = useNavigate();

  useEffect(() => {
    setAuthUser(null);
    navigate("/");
  }, [logout]);

  return (
    <UserContext.Provider value={authUser}>
      <ComponentContext.Provider value={{ components, setComponents }}>
        <div>
          <Navigation setLogOut={setLogOut} />
          <Routes>
            <Route path="/" element={<HomePage />} />
            <Route
              path="/admin"
              element={<AdminPanel setAuthUser={setAuthUser} />}
            />
            <Route path="/cpu" element={<SelectCPU />} />
            <Route path="/mb" element={<SelectMB />} />
            <Route path="/ram" element={<SelectRAM />} />
            <Route path="/storage" element={<SelectStorage />} />
            <Route path="/gpu" element={<SelectGPU />} />
            <Route path="/case" element={<SelectCase />} />
            <Route path="/psu" element={<SelectPSU />} />
          </Routes>
        </div>
      </ComponentContext.Provider>
    </UserContext.Provider>
  );
}

export default App;
