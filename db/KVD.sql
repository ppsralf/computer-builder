-- phpMyAdmin SQL Dump
-- version 5.1.1deb5ubuntu1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 20, 2023 at 07:58 PM
-- Server version: 8.0.32-0ubuntu0.22.04.2
-- PHP Version: 8.1.2-1ubuntu2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `KVD`
--

-- --------------------------------------------------------

--
-- Table structure for table `COMPUTER_CASE`
--

CREATE TABLE `COMPUTER_CASE` (
  `id` int NOT NULL,
  `name` varchar(50) NOT NULL,
  `manufacturer` int NOT NULL,
  `form_factor` enum('ATX','EATX','Micro ATX','Mini ITX') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `2.5_bay` int NOT NULL,
  `3.5_bay` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `COMPUTER_CASE`
--

INSERT INTO `COMPUTER_CASE` (`id`, `name`, `manufacturer`, `form_factor`, `2.5_bay`, `3.5_bay`) VALUES
(3, '4000D Airflow', 17, 'ATX', 2, 2),
(4, 'H510', 21, 'ATX', 3, 2),
(5, 'O11 Dynamic EVO', 22, 'ATX', 2, 1),
(6, 'iCUE 4000X RGB', 17, 'ATX', 3, 2),
(7, 'MATREXX 40', 27, 'Micro ATX', 1, 2),
(8, 'Core V1', 25, 'Mini ITX', 2, 1),
(10, 'MasterBox NR200P', 24, 'Mini ITX', 3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `CPU`
--

CREATE TABLE `CPU` (
  `id` int NOT NULL,
  `name` varchar(100) NOT NULL,
  `manufacturer` int NOT NULL,
  `core_count` int NOT NULL,
  `TDP` int NOT NULL,
  `socket` varchar(10) NOT NULL,
  `integrated_graphics` tinyint(1) NOT NULL,
  `cooler` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `CPU`
--

INSERT INTO `CPU` (`id`, `name`, `manufacturer`, `core_count`, `TDP`, `socket`, `integrated_graphics`, `cooler`) VALUES
(1, 'Intel Core I9-13900K', 11, 24, 125, 'LGA1700', 1, 0),
(6, 'Ryzen 5 5600X', 2, 6, 65, 'AM4', 0, 0),
(7, 'Ryzen 7 5800X', 2, 8, 105, 'AM4', 1, 1),
(8, 'I5-13600K', 11, 14, 125, 'LGA1700', 1, 0),
(9, 'i7-13700K', 11, 16, 125, 'LGA1700', 0, 0),
(10, 'Ryzen 5 5600', 2, 6, 65, 'AM4', 0, 1),
(11, 'i7-12700K', 11, 12, 125, 'LGA1700', 1, 0),
(12, 'Ryzen 7 5700X', 2, 8, 65, 'AM4', 1, 1),
(13, 'Ryzen 7 7700X', 2, 8, 105, 'AM5', 1, 0),
(14, 'i5-12400F', 11, 6, 65, 'LGA1700', 0, 1),
(15, 'Ryzen 5 5600G', 2, 6, 65, 'AM4', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `GPU`
--

CREATE TABLE `GPU` (
  `id` int NOT NULL,
  `name` varchar(50) NOT NULL,
  `manufacturer` int NOT NULL,
  `chipset` varchar(30) NOT NULL,
  `mem_type` enum('GDDR5','GDDR5X','GDDR6','GDDR6X') NOT NULL,
  `TDP` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `GPU`
--

INSERT INTO `GPU` (`id`, `name`, `manufacturer`, `chipset`, `mem_type`, `TDP`) VALUES
(2, 'MSI GeForce RTX 3060 Ventus 2X', 18, 'GeForce RTX 3060', 'GDDR6', 170),
(3, 'MSI GeForce RTX 3060', 18, 'GeForce RTX 3060', 'GDDR6', 170),
(4, 'Gigabyte GAMING OC GeForce RTX 4070 Ti', 4, 'GeForce RTX 4070 Ti', 'GDDR6X', 285),
(5, 'XFX GTR XXX Radeon RX 580', 28, 'Radeon RX 580', 'GDDR5', 185),
(6, 'ASRock Challenger D OC Radeon RX 6700 XT', 29, 'Radeon RX 6700 XT', 'GDDR6', 230);

-- --------------------------------------------------------

--
-- Table structure for table `MANUFACTURER`
--

CREATE TABLE `MANUFACTURER` (
  `id` int NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `MANUFACTURER`
--

INSERT INTO `MANUFACTURER` (`id`, `name`) VALUES
(2, 'AMD'),
(3, 'Asus'),
(4, 'Gigabyte'),
(11, 'Intel'),
(17, 'Corsair'),
(18, 'MSI'),
(21, 'NZXT'),
(22, 'Lian Li'),
(23, 'Phanteks'),
(24, 'Cooler Master'),
(25, 'Thermaltake'),
(26, 'Fractal Design'),
(27, 'Deepcool'),
(28, 'XFX'),
(29, 'ASRock'),
(30, 'Crucial'),
(31, 'Samsung'),
(32, 'Western Digital'),
(33, 'Seagate'),
(34, 'G.Skill'),
(35, 'Silicon Power'),
(36, 'Kingston'),
(37, 'EVGA');

-- --------------------------------------------------------

--
-- Table structure for table `MEMORY`
--

CREATE TABLE `MEMORY` (
  `id` int NOT NULL,
  `name` varchar(100) NOT NULL,
  `manufacturer` int NOT NULL,
  `type` varchar(10) NOT NULL,
  `modules` int NOT NULL,
  `size` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `MEMORY`
--

INSERT INTO `MEMORY` (`id`, `name`, `manufacturer`, `type`, `modules`, `size`) VALUES
(4, 'Corsair Vengeance 32GB (2 x 16 GB)', 17, 'DDR5', 2, 16),
(5, 'Corsair Vengeance RGB Pro SL 32 GB (2 x 16 GB)', 17, 'DDR4', 2, 16),
(6, 'Kingston FURY Beast RGB 16 GB (2 x 8 GB)', 36, 'DDR4', 2, 8),
(7, 'Corsair Dominator Platinum RGB 32 GB (2 x 16 GB)', 17, 'DDR5', 2, 16),
(8, 'G.Skill Ripjaws V 16 GB (2 x 8 GB)', 34, 'DDR4', 2, 8),
(9, 'Kingston FURY Beast 32 GB (2 x 16 GB)', 36, 'DDR5', 2, 16),
(10, 'Kingston FURY Beast 16 GB (2 x 8 GB)', 36, 'DDR4', 2, 8);

-- --------------------------------------------------------

--
-- Table structure for table `MOTHERBOARD`
--

CREATE TABLE `MOTHERBOARD` (
  `id` int NOT NULL,
  `name` varchar(50) NOT NULL,
  `manufacturer` int NOT NULL,
  `socket` varchar(20) NOT NULL,
  `form_factor` enum('ATX','EATX','Micro ATX','Mini ITX') NOT NULL,
  `mem_max` int NOT NULL,
  `mem_type` enum('DDR3','DDR4','DDR5') NOT NULL,
  `mem_slots` int NOT NULL,
  `SATA_ports` int NOT NULL,
  `wifi` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `MOTHERBOARD`
--

INSERT INTO `MOTHERBOARD` (`id`, `name`, `manufacturer`, `socket`, `form_factor`, `mem_max`, `mem_type`, `mem_slots`, `SATA_ports`, `wifi`) VALUES
(2, 'Asus TUF GAMING X570-PLUS', 3, 'AM4', 'ATX', 128, 'DDR4', 4, 8, 1),
(3, 'MSI MAG B550 TOMAHAWK', 18, 'AM4', 'ATX', 128, 'DDR4', 4, 6, 0),
(4, 'MSI B550-A PRO', 18, 'AM4', 'ATX', 128, 'DDR4', 4, 6, 0),
(5, 'Gigabyte Z790 AORUS ELITE AX', 4, 'LGA1700', 'ATX', 128, 'DDR5', 4, 6, 1),
(6, 'MSI MAG B660 TOMAHAWK', 18, 'LGA1700', 'ATX', 128, 'DDR4', 4, 6, 1),
(7, 'Asus ROG STRIX Z690-A GAMING', 3, 'LGA1700', 'ATX', 128, 'DDR4', 4, 6, 1),
(8, 'Gigabyte B450M DS3H', 4, 'AM4', 'Micro ATX', 64, 'DDR4', 4, 4, 1),
(9, 'Asus ROG STRIX Z690-E', 3, 'LGA1700', 'ATX', 128, 'DDR5', 4, 6, 1),
(10, 'MSI MAG Z690 TOMAHAWK', 18, 'LGA1700', 'ATX', 128, 'DDR4', 4, 6, 1),
(11, 'MSI PRO Z690-A', 18, 'LGA1700', 'ATX', 128, 'DDR5', 4, 6, 0),
(12, 'Gigabyte B650M AORUS ELITE', 4, 'AM5', 'Micro ATX', 128, 'DDR5', 4, 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `PSU`
--

CREATE TABLE `PSU` (
  `id` int NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `manufacturer` int NOT NULL,
  `wattage` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `PSU`
--

INSERT INTO `PSU` (`id`, `name`, `manufacturer`, `wattage`) VALUES
(5, 'Corsair RM850x', 17, 850),
(6, 'Corsair RM1000x', 17, 1000),
(7, 'Corsair CX750M', 17, 750),
(8, 'Thermaltake Smart BM2 550 W', 25, 550),
(9, 'Thermaltake Smart BM2 650 W', 25, 650),
(10, 'EVGA SuperNOVA 750 GT', 37, 750),
(11, 'EVGA SuperNOVA 1000 GT', 37, 1000),
(12, 'MSI A1000G PCIE5', 18, 1000),
(13, 'barosanas bloks', 11, 340);

-- --------------------------------------------------------

--
-- Table structure for table `STORAGE`
--

CREATE TABLE `STORAGE` (
  `id` int NOT NULL,
  `name` varchar(30) NOT NULL,
  `manufacturer` int NOT NULL,
  `capacity` int NOT NULL,
  `type` enum('HDD','SSD') NOT NULL,
  `form_factor` enum('2.5','3.5') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `STORAGE`
--

INSERT INTO `STORAGE` (`id`, `name`, `manufacturer`, `capacity`, `type`, `form_factor`) VALUES
(6, 'Crucial MX500 4 TB', 30, 4000, 'SSD', '2.5'),
(7, 'Samsung 860 EVO 1 TB', 31, 1000, 'SSD', '2.5'),
(10, 'Seagate BarraCuda 1 TB', 33, 1000, 'HDD', '3.5'),
(11, 'Western Digital Blue 1 TB', 32, 1000, 'HDD', '3.5'),
(12, 'Samsung 870 Evo 1 TB', 31, 1000, 'SSD', '2.5'),
(13, 'Seagate Barracuda Compute 2 TB', 33, 2000, 'HDD', '3.5'),
(14, 'test', 29, 2, 'SSD', '2.5');

-- --------------------------------------------------------

--
-- Table structure for table `USERS`
--

CREATE TABLE `USERS` (
  `id` int NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `USERS`
--

INSERT INTO `USERS` (`id`, `username`, `password`) VALUES
(20, 'admin', '$2y$10$uGHQ2f9rOOC.IVdsdN.l6ehCzfNRw/ZofxgUU/PSRcqcIgP2QbutW'),
(36, 'ralfs', '$2y$10$SymMF6DhjmnXlq6ENh/JIOjbzNXOIys6EULZbSpAFNrtu0VoFCK6K');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `COMPUTER_CASE`
--
ALTER TABLE `COMPUTER_CASE`
  ADD PRIMARY KEY (`id`),
  ADD KEY `removecc` (`manufacturer`);

--
-- Indexes for table `CPU`
--
ALTER TABLE `CPU`
  ADD PRIMARY KEY (`id`),
  ADD KEY `manufacturer` (`manufacturer`),
  ADD KEY `socket` (`socket`);

--
-- Indexes for table `GPU`
--
ALTER TABLE `GPU`
  ADD PRIMARY KEY (`id`),
  ADD KEY `manufacturer` (`manufacturer`),
  ADD KEY `chipset` (`chipset`);

--
-- Indexes for table `MANUFACTURER`
--
ALTER TABLE `MANUFACTURER`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `MEMORY`
--
ALTER TABLE `MEMORY`
  ADD PRIMARY KEY (`id`),
  ADD KEY `manufacturer` (`manufacturer`);

--
-- Indexes for table `MOTHERBOARD`
--
ALTER TABLE `MOTHERBOARD`
  ADD PRIMARY KEY (`id`),
  ADD KEY `manufacturer` (`manufacturer`);

--
-- Indexes for table `PSU`
--
ALTER TABLE `PSU`
  ADD PRIMARY KEY (`id`),
  ADD KEY `manufacturer` (`manufacturer`);

--
-- Indexes for table `STORAGE`
--
ALTER TABLE `STORAGE`
  ADD PRIMARY KEY (`id`),
  ADD KEY `manufacturer` (`manufacturer`);

--
-- Indexes for table `USERS`
--
ALTER TABLE `USERS`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `COMPUTER_CASE`
--
ALTER TABLE `COMPUTER_CASE`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `CPU`
--
ALTER TABLE `CPU`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `GPU`
--
ALTER TABLE `GPU`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `MANUFACTURER`
--
ALTER TABLE `MANUFACTURER`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `MEMORY`
--
ALTER TABLE `MEMORY`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `MOTHERBOARD`
--
ALTER TABLE `MOTHERBOARD`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `PSU`
--
ALTER TABLE `PSU`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `STORAGE`
--
ALTER TABLE `STORAGE`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `USERS`
--
ALTER TABLE `USERS`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `COMPUTER_CASE`
--
ALTER TABLE `COMPUTER_CASE`
  ADD CONSTRAINT `removecc` FOREIGN KEY (`manufacturer`) REFERENCES `MANUFACTURER` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `CPU`
--
ALTER TABLE `CPU`
  ADD CONSTRAINT `CPU_ibfk_1` FOREIGN KEY (`manufacturer`) REFERENCES `MANUFACTURER` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `GPU`
--
ALTER TABLE `GPU`
  ADD CONSTRAINT `GPU_ibfk_1` FOREIGN KEY (`manufacturer`) REFERENCES `MANUFACTURER` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `MEMORY`
--
ALTER TABLE `MEMORY`
  ADD CONSTRAINT `MEMORY_ibfk_1` FOREIGN KEY (`manufacturer`) REFERENCES `MANUFACTURER` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `MOTHERBOARD`
--
ALTER TABLE `MOTHERBOARD`
  ADD CONSTRAINT `MOTHERBOARD_ibfk_1` FOREIGN KEY (`manufacturer`) REFERENCES `MANUFACTURER` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `PSU`
--
ALTER TABLE `PSU`
  ADD CONSTRAINT `PSU_ibfk_1` FOREIGN KEY (`manufacturer`) REFERENCES `MANUFACTURER` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `STORAGE`
--
ALTER TABLE `STORAGE`
  ADD CONSTRAINT `STORAGE_ibfk_1` FOREIGN KEY (`manufacturer`) REFERENCES `MANUFACTURER` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
