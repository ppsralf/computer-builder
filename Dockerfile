# Use the official PHP image as the base
FROM php:7.4-apache

# Set the working directory
WORKDIR /var/www/html

# Copy PHP application files
COPY php/ .

# Install PHP dependencies if required'
# RUN composer install
RUN docker-php-ext-install pdo pdo_mysql

# Expose the Apache port
EXPOSE 80

# Start Apache web server
CMD ["apache2-foreground"]
