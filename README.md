# Datoru komplektēšanas sistēma

Šī aplikācija tiek veidota kā kvalifikācijas darbs Jēkabpils Agrobiznesa Koledžai.

Pieaugot spēļu un citu prasīgu lietojumprogrammu skaitam, pieaug interese par īpaši pielāgotiem datoriem, lai nodrošinātu optimizētu veiktspēju. Šī lietojumprogramma būs noderīga lietotājiem, kuri vēlas sakomplektēt savas sistēmas un kuriem nepieciešama palīdzība, lai noteiktu, kuras sastāvdaļas ir saderīgas.

##### ❗ Lietotnes izstrāde vēl nav pabeigta, tādēļ tā var izskatīties nepabeigta.
##### 🌐 Lietotne ir pieejama šeit: [Computer Builder](http://ec2-54-242-254-141.compute-1.amazonaws.com/)
###### Pieejai administratora panelim - Lietotājvārds: admin, Parole: P@ssw0rd

## Darba mērķis

Sistēmas galvenais mērķis ir nodrošināt lietotājiem noderīgu rīku datora detaļu saderības noteikšanai. Tas lietotājiem atvieglotu sistēmu komplektēšanu un atjaunināšanu, tādējādi uzlabojot veiktspēju.

## Izmantotās tehnoloģijas

* **Frontend**: React, SCSS
* **Backend**: PHP
* **Datubāze**: MySQL
* **Web serveris**: Nginx
* **Datubāzes administrācijas rīks**: PHPMyAdmin

## Funkcijas

* **Komponentu pārvaldība**: Administrātora vadības panelī administrators var pievienot vai dzēst komponentes. Nospiežot attiecīgās komponentes "Pārvaldības" pogu, administrators var piekļūt komponentei veltītajai lapai un veikt nepieciešamās darbības.
* **Sistēmas komplektēšana un pārbaude**: Lietotāji var izvēlēties komponentes, nospiežot "+ Select (komponentes nosaukums)" pogu, kas atver pieejamo komponentu sarakstu. Nospiežot pogu "+ Select", izvēlētā komponente tiek pievienota sistēmai. Kad visas komponentes ir izvēlētas, lietotāji var nospiest pogu "Check Components", lai skatītu aptuveno enerģijas patēriņu un saņemtu paziņojumus par iespējamām saderības problēmām. Poga "Clear Components" atiestata komponentu sarakstu, lai lietotāji varētu sākt no jauna.
